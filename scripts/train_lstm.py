#!/usr/bin/env python

"""
Feed forward net training using the Lasagne library:
https://github.com/Lasagne
"""

from __future__ import print_function
import argparse
import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T

import lasagne

import data_io_func
import NN_func

theano.config.floatX = 'float32'


##########################################################################
#  FUNCTIONS
##########################################################################


############################### Batch iterator ###############################
# This is just a simple helper function iterating over training data in
# mini-batches of a particular size, optionally in random order. It assumes
# data is available as numpy arrays. For big datasets, you could load numpy
# arrays as memory-mapped files (np.load(..., mmap_mode='r')), or write your
# own custom data iteration function. For small datasets, you can also copy
# them to GPU at once for slightly improved performance. This would involve
# several changes in the main program, though, and is not demonstrated here.
def iterate_minibatches_combine(pep, sh2, targets, batchsize):
    assert pep.shape[0] == sh2.shape[0] == targets.shape[0]
    # shuffle:
    indices = np.arange(len(pep))
    np.random.shuffle(indices)
    for start_idx in range(0, len(pep) - batchsize + 1, batchsize):
        excerpt = indices[start_idx:start_idx + batchsize]
        yield pep[excerpt], sh2[excerpt], targets[excerpt]


def iterate_minibatches_separate(pep, sh2, sh2_mask, targets, batchsize):
    assert pep.shape[0] == sh2.shape[0] == sh2_mask.shape[0] == targets.shape[0]
    # shuffle:
    indices = np.arange(len(pep))
    np.random.shuffle(indices)
    for start_idx in range(0, len(pep) - batchsize + 1, batchsize):
        excerpt = indices[start_idx:start_idx + batchsize]
        yield pep[excerpt], sh2[excerpt], sh2_mask[excerpt], targets[excerpt]


################################################################################
#  PARSE COMMANDLINE OPTIONS
################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-training_data', '--trainfile',  help="file with training data")
parser.add_argument('-validation_data', '--valfile',  help="file with validation data (for early stopping)")
parser.add_argument('-domain_file', '--domain_file',  help="file with SH2 domain sequences")
parser.add_argument('-model_out', '--modelfile',  help="file to store best model")
parser.add_argument('-max_pep_seq_length', '--max_pep_seq_length',
                    help="maximal peptide sequence length, default = -1", default=-1)
parser.add_argument('-encoding', '--encoding',  help="encoding, default = sparse", default="sparse")
parser.add_argument('-blosum', '--blosum', help="file with BLOSUM matrix")
parser.add_argument('-batch_size', '--batch_size',  help="Mini batch size, default = 20", default=20)
parser.add_argument('-epochs', '--epochs',  help="Number of training epochs, default = 200", default=200)
parser.add_argument('-learning_rate', '--learning_rate',  help="Learning rate, default = 0.01", default=0.01)
parser.add_argument('-update', '--update',  help="Method for weight update, default = sgd", default="sgd")
parser.add_argument('-activation', '--activation',  help="Activation function, default = sigmoid", default="sigmoid")
parser.add_argument('-dropout', '--dropout',  help="Dropout (0-0.99 corresponding to 0-99%), default = 0", default=0)
parser.add_argument('-n_hid', '--n_hid',  help="Size of 1st hidden layer, default = 60", default=60)
parser.add_argument('-n_lstm', '--n_lstm',  help="Size of LSTM layer, default = 60", default=60)
parser.add_argument('-architecture', '--architecture',  help="Neural network architecture, default = feed_forward",
                    default="feed_forward")
parser.add_argument('-w_init', '--w_init',  help="Weight initialization, default = glorot_uniform",
                    default="glorot_uniform")
parser.add_argument('-cost_function', '--cost_function',  help="Cost function, default = squared_error",
                    default="squared_error")
parser.add_argument('-seed', '--seed',  help="Seed for random number init., default = -1", default=-1)
parser.add_argument('-n_aa', '--n_aa',  help="number of amino acids to present in each LSTM time step, default = 1",
                    default=1)
parser.add_argument('-no_early_stop', '--no_early_stop', action="store_true",
                    help="Turn off early stopping, default = False", default=False)
parser.add_argument('-continue_training', '--continue_training', action="store_true",
                    help="Continue training, default = False", default=False)
parser.add_argument('-separate', '--separate_pep_sh2', action="store_true",
                    help="Separate the peptide and SH2 so only SH2 goes though the LSTM layer, default = False",
                    default=False)
parser.add_argument('-loo', '--leaveoneout', help="SH2 domain name for domain to leave out of training.", default=None)
args = parser.parse_args()


# get training data file
if args.trainfile is not None:
    trainfile = args.trainfile
    print("# Training data file: " + args.trainfile)
else:
    sys.stderr.write("Please specify training data file!\n")
    sys.exit(2)

# get validation data file
if args.valfile is not None:
    valfile = args.valfile
    print("# Validation data file: " + args.valfile)
else:
    sys.stderr.write("Please specify validation data file!\n")
    sys.exit(2)

# get validation data file
if args.domain_file is not None:
    domain_file = args.domain_file
    print("# Domain file: " + args.domain_file)
else:
    sys.stderr.write("Please specify domain file!\n")
    sys.exit(2)

if args.modelfile is not None:
    modelfile = open(args.modelfile, 'w')
else:
    sys.stderr.write("Please specify modelfile!\n")
    sys.exit(2)

try:
    MAX_PEP_SEQ_LEN = int(args.max_pep_seq_length)
    print("# max. peptide sequence length: " + str(MAX_PEP_SEQ_LEN))
except argparse.ArgumentError:
    sys.stderr.write("Problem with max. peptide sequence length specification (option -max_pep_seq_length)!\n")
    sys.exit(2)

try:
    ENCODING = args.encoding
    print("# encoding: " + str(ENCODING))
except argparse.ArgumentError:
    sys.stderr.write("Problem with sequence encoding specification (option -encoding)!\n")
    sys.exit(2)

if ENCODING == "blosum":
    try:
        blosumfile = args.blosum
        print("# Blosum matrix file: " + str(blosumfile))
    except argparse.ArgumentError:
        sys.stderr.write("Blosum encoding requires blosum matrix file! (option -blosum)!\n")
        sys.exit(2)

try:
    BATCH_SIZE = int(args.batch_size)
    print("# batch size: " + str(BATCH_SIZE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with mini batch size specification (option -batch_size)!\n")
    sys.exit(2)

try:
    EPOCHS = range(1, int(args.epochs)+1)
    print("# number of training epochs: " + str(args.epochs))
except argparse.ArgumentError:
    sys.stderr.write("Problem with epochs specification (option -epochs)!\n")
    sys.exit(2)

try:
    LEARNING_RATE = float(args.learning_rate)
    print("# learning rate: " + str(LEARNING_RATE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with learning rate specification (option -learning_rate)!\n")
    sys.exit(2)

try:
    UPDATE = args.update
    print("# weight update method: " + str(UPDATE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with update specification (option -update)!\n")
    sys.exit(2)

try:
    ACTIVATION = args.activation
    print("# activation function: " + str(ACTIVATION))
except argparse.ArgumentError:
    sys.stderr.write("Problem with activation function specification (option -activation)!\n")
    sys.exit(2)

try:
    DROPOUT = float(args.dropout)
    print("# dropout: " + str(DROPOUT))
except argparse.ArgumentError:
    sys.stderr.write("Problem with dropout specification (option -dropout)!\n")
    sys.exit(2)

try:
    N_HID = int(args.n_hid)
    print("# number of hidden units: " + str(N_HID))
except argparse.ArgumentError:
    sys.stderr.write("Problem with number of hidden neurons specification (option -n_hid)!\n")
    sys.exit(2)


try:
    N_LSTM = int(args.n_lstm)
    print("# number of LSTM units: " + str(N_LSTM))
except argparse.ArgumentError:
    sys.stderr.write("Problem with number of LSTM neurons specification (option -n_lstm)!\n")
    sys.exit(2)

try:
    ARCHITECTURE = args.architecture
    print("# architecture: " + str(ARCHITECTURE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with architecture specification (option -architecture)!\n")
    sys.exit(2)

try:
    W_INIT = args.w_init
    print("# w_init: " + str(W_INIT))
except argparse.ArgumentError:
    sys.stderr.write("Problem with weight initialization specification (option -w_init)!\n")
    sys.exit(2)

try:
    COST_FUNCTION = args.cost_function
    print("# cost_function: " + str(COST_FUNCTION))
except argparse.ArgumentError:
    sys.stderr.write("Problem with cost function specification (option -cost_function)!\n")
    sys.exit(2)

try:
    SEED = int(args.seed)
    print("# seed: " + str(SEED))
except argparse.ArgumentError:
    sys.stderr.write("Problem with seed specification (option -seed)!\n")
    sys.exit(2)

try:
    N_AA = int(args.n_aa)
    print("# number of AA / time step: " + str(N_AA))
except argparse.ArgumentError:
    sys.stderr.write("Problem with number of AA / time step specification (option -n_aa)!\n")
    sys.exit(2)

NO_EARLY_STOP = args.no_early_stop
if NO_EARLY_STOP:
    print("# Early stopping is turned OFF")
else:
    print("# Early stopping is turned ON")

CONTINUE = args.continue_training
if CONTINUE:
    print("# Training is CONTINUED")
else:
    print("# New training (NOT continuing old training)")

SEPARATE = args.separate_pep_sh2
if SEPARATE:
    print("# Separating peptide and SH2 so only SH2 goes through the LSTM layer")
else:
    print("# Combining peptide and SH2 so only both go through the LSTM layer")

LEAVE_ONE_OUT = args.leaveoneout
print("# Leave-one-out SH2 domain: " + args.leaveoneout)


################################################################################
#  MAIN
################################################################################

if SEED != -1:
    print("# Setting seed for random number generation...")
    lasagne.random.set_rng(np.random.RandomState(seed=SEED))
    np.random.seed(seed=SEED)  # for shuffling training examples

print("# Loading data...")

sh2_seq = data_io_func.read_sh2_seq(domain_file)
# encode data (list of numpy nd-arrays):
if ENCODING == "sparse":
    X_pep_train, X_sh2_train, y_train = data_io_func.enc_sparse_pep_sh2(trainfile, sh2_seq, LEAVE_ONE_OUT)
    X_pep_val, X_sh2_val, y_val = data_io_func.enc_sparse_pep_sh2(valfile, sh2_seq, LEAVE_ONE_OUT)
elif ENCODING == "blosum":
    blosum = data_io_func.read_blosum_MN(blosumfile)
    X_pep_train, X_sh2_train, y_train = data_io_func.enc_blosum_pep_sh2(trainfile, sh2_seq, LEAVE_ONE_OUT, blosum)
    X_pep_val, X_sh2_val, y_val = data_io_func.enc_blosum_pep_sh2(valfile, sh2_seq, LEAVE_ONE_OUT, blosum)
else:
    sys.stderr.write("Encoding has to be either sparse or blosum.\n")
    sys.exit(2)

# get target length:
T_LEN = y_train[0].shape[0]

# always take max SH2 seq length, even if peptide sequence length is restricted:
MAX_SH2_SEQ_LEN = max(len(max(X_sh2_train, key=len)), len(max(X_sh2_val, key=len)))

if MAX_PEP_SEQ_LEN == -1:
    # no length restraint -> find max length in data set
    MAX_PEP_SEQ_LEN = max(len(max(X_pep_train, key=len)), len(max(X_pep_val, key=len)))
else:
    # remove peptides with length longer than max peptide length:
    idx = [i for i, x in enumerate(X_pep_train) if len(x) > MAX_PEP_SEQ_LEN]
    X_pep_train = [i for j, i in enumerate(X_pep_train) if j not in idx]
    X_sh2_train = [i for j, i in enumerate(X_sh2_train) if j not in idx]
    y_train = [i for j, i in enumerate(y_train) if j not in idx]

    idx = [i for i, x in enumerate(X_pep_val) if len(x) > MAX_PEP_SEQ_LEN]
    X_pep_val = [i for j, i in enumerate(X_pep_val) if j not in idx]
    X_sh2_val = [i for j, i in enumerate(X_sh2_val) if j not in idx]
    y_val = [i for j, i in enumerate(y_val) if j not in idx]


# save sequences as np.ndarray instead of list of np.ndarrays:
# for LSTM we need pep + space + MHC and mask
if SEPARATE:
    X_pep_train, X_sh2_train, X_train_mask = \
        data_io_func.pad_pep_sh2_mask_multi_separate(X_pep_train, X_sh2_train, MAX_PEP_SEQ_LEN, MAX_SH2_SEQ_LEN, N_AA)
    X_pep_val, X_sh2_val, X_val_mask = \
        data_io_func.pad_pep_sh2_mask_multi_separate(X_pep_val, X_sh2_val, MAX_PEP_SEQ_LEN, MAX_SH2_SEQ_LEN, N_AA)
    N_PEP_FEATURES = X_pep_train.shape[2]
    # features of sh2 used in shaping LSTM layer. This is not 21 if n_aa is more than 1
    N_SH2_FEATURES = X_sh2_train.shape[2]
else:
    X_train, X_train_mask = \
        data_io_func.pad_pep_sh2_mask_multi(X_pep_train, X_sh2_train, MAX_PEP_SEQ_LEN, MAX_SH2_SEQ_LEN, N_AA)
    X_val, X_val_mask = \
        data_io_func.pad_pep_sh2_mask_multi(X_pep_val, X_sh2_val, MAX_PEP_SEQ_LEN, MAX_SH2_SEQ_LEN, N_AA)
    N_PEP_FEATURES = X_train.shape[2]
    N_SH2_FEATURES = X_train.shape[2]

    print("# X_train shape: " + str(X_train.shape))
    print("# X_train_mask shape: " + str(X_train_mask.shape))


y_train = data_io_func.pad_seqs(y_train, T_LEN)
y_val = data_io_func.pad_seqs(y_val, T_LEN)

print("# N_PEP_FEATURES: " + str(N_PEP_FEATURES))
print("# N_SH2_FEATURES: " + str(N_SH2_FEATURES))
print("# y_train shape: " + str(y_train.shape))

# Prepare Theano variables for targets and learning rate:
sym_target = T.vector('targets', dtype='float32')
sym_l_rate = T.scalar()

# Build the network:
print("# Building the network...")

if ARCHITECTURE == "lstm":
    if SEPARATE:
        network, in_pep, in_sh2, in_sh2_mask = \
            NN_func.build_lstm(n_pep_features=N_PEP_FEATURES, n_sh2_features=N_SH2_FEATURES, n_lstm=N_LSTM,
                               activation=ACTIVATION, dropout=DROPOUT, n_hid=N_HID, w_init=W_INIT,
                               n_pep_len=MAX_PEP_SEQ_LEN, separate=SEPARATE)
    else:
        network, in_pep_sh2, in_pep_sh2_mask = \
            NN_func.build_lstm(n_pep_features=N_PEP_FEATURES, n_sh2_features=N_SH2_FEATURES, n_lstm=N_LSTM,
                               activation=ACTIVATION, dropout=DROPOUT, n_hid=N_HID, w_init=W_INIT,
                               n_pep_len=MAX_PEP_SEQ_LEN, separate=SEPARATE)
    architecture = np.array([ARCHITECTURE])
    net_hyper_params = np.array([N_PEP_FEATURES, N_SH2_FEATURES, N_LSTM, ACTIVATION, DROPOUT, N_HID, W_INIT, SEPARATE])
else:
    sys.stderr.write("Unknown architecture specified (option -architecture)!\n")
    sys.exit(2)

print("# params: " + str(lasagne.layers.get_all_params(network)))
weights = list(lasagne.layers.get_all_params(network))
weights = [str(e) for e in weights]
w = [i for i, j in enumerate(weights) if j == 'W' or j == 'W_in_to_ingate' or j == 'W_hid_to_ingate'
     or j == 'W_in_to_forgetgate' or j == 'W_hid_to_forgetgate' or j == 'W_in_to_cell' or j == 'W_hid_to_cell'
     or j == 'W_in_to_outgate' or j == 'W_hid_to_outgate']
print("# weights: " + str(w))
print("# number of layers: " + str(len(lasagne.layers.get_all_layers(network))))
print("# number of parameters: " + str(lasagne.layers.count_params(network)))

# INITIALIZE VARIABLES ---------------------------------------------------------

if CONTINUE:
    # get current parameters:
    params = lasagne.layers.get_all_param_values(network)
    old_params = np.load(modelfile)['arr_0']

    # check if dimensions match:
    assert len(old_params) == len(params)
    for j in range(0, len(old_params)):
        assert old_params[j].shape == params[j].shape
    # set parameters in network:
    lasagne.layers.set_all_param_values(network, old_params)

print("# Compiling Theano training and validation functions...")

# TRAINING FUNCTION -----------------------------------------------------------

prediction = lasagne.layers.get_output(network)

# loss function:
if COST_FUNCTION == "squared_error":
    loss = lasagne.objectives.squared_error(prediction.flatten(), sym_target)
else:
    sys.stderr.write("Unknown cost function specified (option -cost_function)!\n")
    sys.exit(2)
loss = loss.mean()

# update:
params = lasagne.layers.get_all_params(network, trainable=True)

# constrain gradients for LSTM:
grads = T.grad(loss, params)
scaled_grads = lasagne.updates.total_norm_constraint(grads, max_norm=20)

if UPDATE == "sgd":
    updates = lasagne.updates.sgd(scaled_grads, params, learning_rate=sym_l_rate)
elif UPDATE == "rmsprop":
    updates = lasagne.updates.rmsprop(scaled_grads, params, learning_rate=sym_l_rate)
elif UPDATE == "adam":
    updates = lasagne.updates.adam(scaled_grads, params, learning_rate=sym_l_rate)
elif UPDATE == "adadelta":
    updates = lasagne.updates.adadelta(scaled_grads, params, learning_rate=sym_l_rate)
else:
    sys.stderr.write("Unknown update specified (option -update)!\n")
    sys.exit(2)

# compile training function
if SEPARATE:
    train_fn = theano.function([in_pep.input_var, in_sh2.input_var, in_sh2_mask.input_var, sym_target, sym_l_rate],
                               loss, updates=updates, on_unused_input='warn')
else:
    train_fn = theano.function([in_pep_sh2.input_var, in_pep_sh2_mask.input_var, sym_target, sym_l_rate], loss,
                               updates=updates, on_unused_input='warn')


# VALIDATION FUNCTION ----------------------------------------------------------
test_prediction = lasagne.layers.get_output(network, deterministic=True)

if COST_FUNCTION == "squared_error":
    test_loss = lasagne.objectives.squared_error(test_prediction.flatten(), sym_target)
else:
    sys.stderr.write("Unknown cost function (option -cost_function)!\n")
    sys.exit(2)
test_loss = test_loss.mean()


# compile validation function:
if SEPARATE:
    val_fn = theano.function([in_pep.input_var, in_sh2.input_var, in_sh2_mask.input_var, sym_target], test_loss,
                             on_unused_input='warn')
else:
    val_fn = theano.function([in_pep_sh2.input_var, in_pep_sh2_mask.input_var, sym_target], test_loss,
                             on_unused_input='warn')

# TRAINING LOOP:----------------------------------------------------------------
start_time = time.time()

print("# Start training loop...")

if not NO_EARLY_STOP:
    b_epoch = 0
    b_train_err = 99
    b_val_err = 99

for e in EPOCHS:

    train_err = 0
    train_batches = 0
    val_err = 0
    val_batches = 0
    e_start_time = time.time()

    if SEPARATE:
        # shuffle training examples and iterate through minbatches:
        for batch in iterate_minibatches_separate(X_pep_train, X_sh2_train, X_train_mask, y_train, BATCH_SIZE):
            pep, sh2, sh2_mask, targets = batch
            train_err += train_fn(pep, sh2, sh2_mask, targets.flatten(), LEARNING_RATE)
            train_batches += 1

        # predict validation set:
        for batch in iterate_minibatches_separate(X_pep_val, X_sh2_val, X_val_mask, y_val, BATCH_SIZE):
            pep, sh2, sh2_mask, targets = batch
            val_err += val_fn(pep, sh2, sh2_mask, targets.flatten())
            val_batches += 1
    else:
        # shuffle training examples and iterate through minbatches:
        for batch in iterate_minibatches_combine(X_train, X_train_mask, y_train, BATCH_SIZE):
            pep_sh2, pep_sh2_mask, targets = batch
            train_err += train_fn(pep_sh2, pep_sh2_mask, targets.flatten(), LEARNING_RATE)
            train_batches += 1

        # predict validation set:
        for batch in iterate_minibatches_combine(X_val, X_val_mask, y_val, BATCH_SIZE):
            pep_sh2, pep_sh2_mask, targets = batch
            val_err += val_fn(pep_sh2, pep_sh2_mask, targets.flatten())
            val_batches += 1

    # save model:
    if NO_EARLY_STOP:
        np.savez(args.modelfile, lasagne.layers.get_all_param_values(network), architecture, net_hyper_params)
    else:
        # use early stopping, save only best model:
        if (val_err/val_batches) < b_val_err:
            np.savez(args.modelfile, lasagne.layers.get_all_param_values(network), architecture, net_hyper_params)
            b_val_err = val_err/val_batches
            b_train_err = train_err/train_batches
            b_epoch = e

    # print performance:
    print("Epoch " + str(e) + "\ttraining error: " + str(round(train_err/train_batches, 4)) +
          "\tvalidation error: " + str(round(val_err/val_batches, 4)) +
          "\ttime: " + str(round(time.time()-e_start_time, 3)) + " s")


# print best performance:
if not NO_EARLY_STOP:
    print("# Best epoch: " + str(b_epoch) + "\ttrain error: " + str(round(b_train_err, 4)) +
          "\tvalidation error: " + str(round(b_val_err, 4)))
# report total time used for training:
print("# Time for training: " + str(round((time.time()-start_time)/60, 3)) + " min")
print("# Done!")

