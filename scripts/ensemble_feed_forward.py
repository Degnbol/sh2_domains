#!/usr/bin/env python

"""
Predict data with feed forward net trained using the Lasagne library:
https://github.com/Lasagne
"""

from __future__ import print_function
import argparse
import sys
import os

import numpy as np
from scipy.stats import pearsonr
from sklearn.metrics import roc_auc_score
import theano
import theano.tensor as T

import lasagne

import data_io_func
import NN_func



################################################################################
#	PARSE COMMANDLINE OPTIONS
################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-test_data', '--testfile',  help="file with data to be predicted")
parser.add_argument('-encoding', '--encoding',  help="encoding, default = sparse", default="sparse")
parser.add_argument('-blosum', '--blosum', help="file with BLOSUM matrix")
parser.add_argument('-max_pep_seq_length', '--max_pep_seq_length',  help="max peptide sequence length, default = 20",
                    default=20)
parser.add_argument('-work_dir', '--working_directory',  help="directory to put each domain's work like its model files")
parser.add_argument('-out', '--outfile',  help="file to store output table")
parser.add_argument('-ensemblelist', '--ensemblelist', nargs='+', help="list of files that are the ensemble models")
parser.add_argument('-domain', '--domain', help="A single domain to use, default = None meaning that all domains are used", default=None)
args = parser.parse_args()

# get testfile:
if args.testfile is not None:
    testfile = args.testfile
else:
    sys.stderr.write("Please specify testfile!\n")
    sys.exit(2)

try:
    ENCODING = args.encoding
    print("# encoding: " + str(ENCODING))
except argparse.ArgumentError:
    sys.stderr.write("Problem with sequence encoding specification (option -encoding)!\n")
    sys.exit(2)

if ENCODING == "blosum":
    try:
        blosumfile = args.blosum
        print("# Blosum matrix file: " + str(blosumfile))
    except argparse.ArgumentError:
        sys.stderr.write("Blosum encoding requires blosum matrix file! (option -blosum)!\n")
        sys.exit(2)

try:
    MAX_PEP_SEQ_LEN = int(args.max_pep_seq_length)
except argparse.ArgumentError:
    sys.stderr.write("Problem with max. peptide sequence length specification (option -max_pep_seq_length)!\n")
    sys.exit(2)

# get working directory
if args.working_directory is not None:
    work_dir = args.working_directory
else:
    sys.stderr.write("Please specify working directory!\n")
    sys.exit(2)

# get outputfile:
if args.outfile is not None:
    outfilename = args.outfile
else:
    sys.stderr.write("Please specify output file!\n")
    sys.exit(2)

# get ensemble list:
if args.ensemblelist is not None:
    ensemblelist = args.ensemblelist
else:
    sys.stderr.write("Please specify data file with hyper parameters and weight files!\n")
    sys.exit(2)

domains = [args.domain]

################################################################################
#   LOAD DATA
################################################################################

print("# Loading data...")

# create list with names of all SH2 domains
if domains[0] is None:
    domains = data_io_func.get_sh2s(testfile)

# train for each domain
for domain in domains:
    # check if we are using this domain
    if not os.path.isdir(work_dir + '/' + domain):
        print("# directory does not exist for domain " + domain + ", it is skipped.")
        continue

    print("# Predicting on domain " + domain)
    # find full paths for model files for ensemble
    ensemble = []
    for ensemblename in ensemblelist:
        ensemble.append(work_dir + '/' + domain + '/' + ensemblename)

    # encode data (list of numpy nd-arrays):
    if ENCODING == "sparse":
        X_pep, y = data_io_func.enc_sparse_pep_sh2(testfile, domain, None)
    elif ENCODING == "blosum":
        blosum = data_io_func.read_blosum_MN(blosumfile)
        X_pep, y = data_io_func.enc_blosum_pep_sh2(testfile, domain, None, blosum)
    else:
        sys.stderr.write("Encoding has to be either sparse or blosum.\n")
        sys.exit(2)

    # get target length:
    T_LEN = y[0].shape[0]

    # find max peptide sequence length (if not specified)
    if MAX_PEP_SEQ_LEN == -1:
        # no length restraint -> find max length in dataset
        MAX_PEP_SEQ_LEN = len(max(X_pep, key=len))
    else:
        # remove peptides with length longer than max peptide length:
        idx = [i for i, x in enumerate(X_pep) if len(x) > MAX_PEP_SEQ_LEN]
        X_pep = [i for j, i in enumerate(X_pep) if j not in idx]
        y = [i for j, i in enumerate(y) if j not in idx]

    # save sequences as np.ndarray instead of list of np.ndarrays:
    X = data_io_func.pad_seqs(X_pep, MAX_PEP_SEQ_LEN)
    y = data_io_func.pad_seqs(y, T_LEN)

    # save Amino Acid sequences:
    pep_aa = data_io_func.get_pep_aa_sh2(testfile, domain, MAX_PEP_SEQ_LEN)

    ################################################################################
    #   PREDICT SINGLE NETWORKS
    ################################################################################

    # variable to store predictions in:
    all_pred = np.zeros((len(ensemble), len(X_pep)))

    # go through each net and predict:
    count = 0
    old_hyper_params = ''

    for paramfile in ensemble:

        # LOAD PARAMETERS:----------------------------------------------------------
        # load parameters of best model:
        best_params = np.load(paramfile)['arr_0']
        ARCHITECTURE = np.load(paramfile)['arr_1']
        hyper_params = np.load(paramfile)['arr_2']

        # BUILD NETWORK AND COMPILE TRAINING FUNCTION:------------------------------
        if set(hyper_params) != set(old_hyper_params):
            if ARCHITECTURE == "feed_forward":

                N_FEATURES = int(hyper_params[0])
                SEQ_LEN = int(hyper_params[1])
                ACTIVATION = hyper_params[2]
                DROPOUT = float(hyper_params[3])
                N_HID = int(hyper_params[4])
                print("# N_HID: " + str(N_HID))
                W_INIT = hyper_params[5]

                network, in_pep = NN_func.build_feed_forward(n_features=N_FEATURES, seq_len=SEQ_LEN,
                                                             activation=ACTIVATION, dropout=DROPOUT, n_hid=N_HID,
                                                             w_init=W_INIT)

            else:
                sys.stderr.write("Unknown architecture specified (option -architecture)!\n")
                sys.exit(2)

            # COMPILE PREDICTION FUNCTION-----------------------------------------------
            prediction = lasagne.layers.get_output(network, deterministic=True)

            # compile validation function:
            pred_fn = theano.function([in_pep.input_var], prediction, allow_input_downcast=True)

        print("# Set weights")
        # SET WEIGHTS---------------------------------------------------------------
        # get current parameters:
        params = lasagne.layers.get_all_param_values(network)

        # check if dimensions match:
        assert len(best_params) == len(params)
        for j in range(0, len(best_params)):
            assert best_params[j].shape == params[j].shape
        # set parameters in network:
        lasagne.layers.set_all_param_values(network, best_params)

        print("# Run forward pass")
        # RUN FORWARD PASS----------------------------------------------------------
        # predict validation set:
        all_pred[count] = pred_fn(X).flatten()

        old_hyper_params = hyper_params
        count += 1

    # calculate mean predictions:
    pred = np.mean(all_pred, axis=0)

    ################################################################################
    #   PRINT RESULTS TABLE
    ################################################################################
    print("# Printing results...")
    assert pred.shape[0] == y.shape[0] == len(pep_aa)
    outfile = open(work_dir + '/' + domain + '/' + outfilename, "w")

    outfile.write("peptide\tSH2\tprediction\ttarget\n")
    y = y.flatten()
    for i in range(0, len(pep_aa)):
        outfile.write(pep_aa[i] + "\t" + domain + "\t" + str(pred[i]) + "\t" + str(y[i]) + "\n")

    # calculate PCC:
    pcc, pval = pearsonr(pred.flatten(), y.flatten())
    # calculate AUC:
    y_binary = y >= 0.5
    if all(y_binary) or not any(y_binary):
        auc = "nan"
    else:
        auc = roc_auc_score(y_binary, pred.flatten())

    outfile.write("# PCC: " + str(pcc) + " p-value: " + str(pval) + " AUC: " + str(auc) + "\n")
    outfile.close()

print("# Done!")
