#!/usr/bin/env python

"""
Feed forward net training using the Lasagne library:
https://github.com/Lasagne
"""

from __future__ import print_function
import argparse
import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T

import lasagne

import data_io_func
import NN_func

theano.config.floatX = 'float32'


##########################################################################
#  FUNCTIONS
##########################################################################


############################### Batch iterator ###############################
# This is just a simple helper function iterating over training data in
# mini-batches of a particular size, optionally in random order. It assumes
# data is available as numpy arrays. For big datasets, you could load numpy
# arrays as memory-mapped files (np.load(..., mmap_mode='r')), or write your
# own custom data iteration function. For small datasets, you can also copy
# them to GPU at once for slightly improved performance. This would involve
# several changes in the main program, though, and is not demonstrated here.
def iterate_minibatches(pep, sh2, targets, batchsize):
    assert pep.shape[0] == sh2.shape[0] == targets.shape[0]
    # shuffle:
    indices = np.arange(len(pep))
    np.random.shuffle(indices)
    for start_idx in range(0, len(pep) - batchsize + 1, batchsize):
        excerpt = indices[start_idx:start_idx + batchsize]
        yield pep[excerpt], sh2[excerpt], targets[excerpt]


################################################################################
#  PARSE COMMANDLINE OPTIONS
################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-training_data', '--trainfile',  help="file with training data")
parser.add_argument('-validation_data', '--valfile',  help="file with validation data (for early stopping)")
parser.add_argument('-domain_file', '--domain_file',  help="file with SH2 domain sequences")
parser.add_argument('-model_out', '--modelfile',  help="file to store best model")
parser.add_argument('-max_pep_seq_length', '--max_pep_seq_length',
                    help="maximal peptide sequence length, default = -1", default=-1)
parser.add_argument('-encoding', '--encoding',  help="encoding, default = sparse", default="sparse")
parser.add_argument('-blosum', '--blosum', help="file with BLOSUM matrix")
parser.add_argument('-batch_size', '--batch_size',  help="Mini batch size, default = 20", default=20)
parser.add_argument('-epochs', '--epochs',  help="Number of training epochs, default = 200", default=200)
parser.add_argument('-learning_rate', '--learning_rate',  help="Learning rate, default = 0.01", default=0.01)
parser.add_argument('-update', '--update',  help="Method for weight update, default = sgd", default="sgd")
parser.add_argument('-activation', '--activation',  help="Activation function, default = sigmoid", default="sigmoid")
parser.add_argument('-dropout_dense', '--dropout_dense',  help="Dropout after dense layer (0-0.99 corresponding to 0-99%), default = 0", default=0)
parser.add_argument('-dropout_cnn', '--dropout_cnn',  help="Dropout after CNN layers (0-0.99 corresponding to 0-99%), default = 0", default=0)
parser.add_argument('-n_hid', '--n_hid',  help="Size of 1st hidden layer, default = 60", default=60)
parser.add_argument('-n_filters', '--n_filters',  help="Number of filters used in the CNN, default = 20", default=20)
parser.add_argument('-filter_sizes', '--filter_sizes', nargs="+", help="Size of the filters in the CNN, default=[3,5,7]",
                    default=[3, 5, 7])
parser.add_argument('-architecture', '--architecture',  help="Neural network architecture, default = feed_forward",
                    default="feed_forward")
parser.add_argument('-w_init', '--w_init',  help="Weight initialization, default = glorot_uniform",
                    default="glorot_uniform")
parser.add_argument('-cost_function', '--cost_function',  help="Cost function, default = squared_error",
                    default="squared_error")
parser.add_argument('-seed', '--seed',  help="Seed for random number init., default = -1", default=-1)
parser.add_argument('-no_early_stop', '--no_early_stop', action="store_true",
                    help="Turn off early stopping, default = False", default=False)
parser.add_argument('-continue_training', '--continue_training', action="store_true",
                    help="Continue training, default = False", default=False)
# calling this argument separate is mostly to keep the same name as used for LSTM.
# The peptide and SH2 domain are kept separate in both cases before concatenation, but if separate is true,
# the peptide is not run through CNN layers.
parser.add_argument('-separate', '--separate', action="store_true",
                    help="Run convolutions on SH2 domain instead of also running convolutions on peptides, default = False", default=False)
parser.add_argument('-merge', '--merge', action="store_true",
                    help="Concatenate peptide and SH2 from the start, making them share CNN layers, default = False", default=False)
parser.add_argument('-loo', '--leaveoneout', help="SH2 domain name for domain to leave out of training.", default=None)
args = parser.parse_args()


# get training data file
if args.trainfile is not None:
    trainfile = args.trainfile
    print("# Training data file: " + args.trainfile)
else:
    sys.stderr.write("Please specify training data file!\n")
    sys.exit(2)

# get validation data file
if args.valfile is not None:
    valfile = args.valfile
    print("# Validation data file: " + args.valfile)
else:
    sys.stderr.write("Please specify validation data file!\n")
    sys.exit(2)

# get domain file
domain_file = args.domain_file
if domain_file is not None:
    print("# Domain file: " + args.domain_file)
else:
    sys.stdout.write("Domain file not specified. Assuming all domains are written as amino acids.\n")

if args.modelfile is not None:
    modelfile = open(args.modelfile, 'w')
else:
    sys.stderr.write("Please specify modelfile!\n")
    sys.exit(2)

try:
    MAX_PEP_SEQ_LEN = int(args.max_pep_seq_length)
    print("# max. peptide sequence length: " + str(MAX_PEP_SEQ_LEN))
except argparse.ArgumentError:
    sys.stderr.write("Problem with max. peptide sequence length specification (option -max_pep_seq_length)!\n")
    sys.exit(2)

try:
    ENCODING = args.encoding
    print("# encoding: " + str(ENCODING))
except argparse.ArgumentError:
    sys.stderr.write("Problem with sequence encoding specification (option -encoding)!\n")
    sys.exit(2)

if ENCODING == "blosum":
    try:
        blosumfile = args.blosum
        print("# Blosum matrix file: " + str(blosumfile))
    except argparse.ArgumentError:
        sys.stderr.write("Blosum encoding requires blosum matrix file! (option -blosum)!\n")
        sys.exit(2)

try:
    BATCH_SIZE = int(args.batch_size)
    print("# batch size: " + str(BATCH_SIZE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with mini batch size specification (option -batch_size)!\n")
    sys.exit(2)

try:
    EPOCHS = range(1, int(args.epochs)+1)
    print("# number of training epochs: " + str(args.epochs))
except argparse.ArgumentError:
    sys.stderr.write("Problem with epochs specification (option -epochs)!\n")
    sys.exit(2)

try:
    LEARNING_RATE = float(args.learning_rate)
    print("# learning rate: " + str(LEARNING_RATE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with learning rate specification (option -learning_rate)!\n")
    sys.exit(2)

try:
    UPDATE = args.update
    print("# weight update method: " + str(UPDATE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with update specification (option -update)!\n")
    sys.exit(2)

try:
    ACTIVATION = args.activation
    print("# activation function: " + str(ACTIVATION))
except argparse.ArgumentError:
    sys.stderr.write("Problem with activation function specification (option -activation)!\n")
    sys.exit(2)

try:
    DROPOUT_DENSE = float(args.dropout_dense)
    print("# dropout dense: " + str(DROPOUT_DENSE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with dropout dense specification (option -dropout)!\n")
    sys.exit(2)

try:
    DROPOUT_CNN = float(args.dropout_cnn)
    print("# dropout: " + str(DROPOUT_CNN))
except argparse.ArgumentError:
    sys.stderr.write("Problem with dropout CNN specification (option -dropout)!\n")
    sys.exit(2)

try:
    N_HID = int(args.n_hid)
    print("# number of hidden units: " + str(N_HID))
except argparse.ArgumentError:
    sys.stderr.write("Problem with number of hidden neurons specification (option -n_hid)!\n")
    sys.exit(2)


try:
    N_FILTERS = int(args.n_filters)
    print("# number of filters in CNN: " + str(N_FILTERS))
except argparse.ArgumentError:
    sys.stderr.write("Problem with number of filters in CNN specification (option -n_filters)!\n")
    sys.exit(2)

try:
    FILTER_SIZES = np.array(args.filter_sizes, dtype=int)
    print("# Filter sizes in CNN: " + str(FILTER_SIZES))
except argparse.ArgumentError:
    sys.stderr.write("Problem with filter sizes in CNN specification (option -filter_sizes)!\n")
    sys.exit(2)

try:
    ARCHITECTURE = args.architecture
    print("# architecture: " + str(ARCHITECTURE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with architecture specification (option -architecture)!\n")
    sys.exit(2)

try:
    W_INIT = args.w_init
    print("# w_init: " + str(W_INIT))
except argparse.ArgumentError:
    sys.stderr.write("Problem with weight initialization specification (option -w_init)!\n")
    sys.exit(2)

try:
    COST_FUNCTION = args.cost_function
    print("# cost_function: " + str(COST_FUNCTION))
except argparse.ArgumentError:
    sys.stderr.write("Problem with cost function specification (option -cost_function)!\n")
    sys.exit(2)

try:
    SEED = int(args.seed)
    print("# seed: " + str(SEED))
except argparse.ArgumentError:
    sys.stderr.write("Problem with seed specification (option -seed)!\n")
    sys.exit(2)

NO_EARLY_STOP = args.no_early_stop
if NO_EARLY_STOP:
    print("# Early stopping is turned OFF")
else:
    print("# Early stopping is turned ON")

CONTINUE = args.continue_training
if CONTINUE:
    print("# Training is CONTINUED")
else:
    print("# New training (NOT continuing old training)")

SEPARATE = args.separate
if SEPARATE:
    print("# Peptide is encoded and NOT run through CNN layers before being concatenated with SH2 domain.")
else:
    print("# Peptide is run through CNN layers just like SH2 domain before they are concatenated.")

MERGE = args.merge
if MERGE:
    print("# Peptide and SH2 are concatenated as the first thing and share CNN layers.")
else:
    print("# Peptide and SH2 are not concatenated as the first thing. Argument separate will determine what is done.")

LEAVE_ONE_OUT = args.leaveoneout
print("# Leave-one-out SH2 domain: " + args.leaveoneout)


################################################################################
#  MAIN
################################################################################

if SEED != -1:
    print("# Setting seed for random number generation...")
    lasagne.random.set_rng(np.random.RandomState(seed=SEED))
    np.random.seed(seed=SEED)  # for shuffling training examples

print("# Loading data...")

if domain_file is None:
    # there will be made no dictionary for SH2 domains
    sh2_seq = None
else:
    # make dictionary of sh2 domains and their AA sequences
    sh2_seq = data_io_func.read_sh2_seq(domain_file)
    # if the SH2 domain to be left out is not in the dictionary, just make it None
    if LEAVE_ONE_OUT not in sh2_seq:
        LEAVE_ONE_OUT = None
        print("# Leave-one-out SH2 domain not found in list of SH2 domains and is set to None.")

# encode data (list of numpy nd-arrays):
if ENCODING == "sparse":
    X_pep_train, X_sh2_train, y_train = data_io_func.enc_sparse_pep_sh2(trainfile, sh2_seq, LEAVE_ONE_OUT)
    X_pep_val, X_sh2_val, y_val = data_io_func.enc_sparse_pep_sh2(valfile, sh2_seq, LEAVE_ONE_OUT)
elif ENCODING == "blosum":
    blosum = data_io_func.read_blosum_MN(blosumfile)
    X_pep_train, X_sh2_train, y_train = data_io_func.enc_blosum_pep_sh2(trainfile, sh2_seq, LEAVE_ONE_OUT, blosum)
    X_pep_val, X_sh2_val, y_val = data_io_func.enc_blosum_pep_sh2(valfile, sh2_seq, LEAVE_ONE_OUT, blosum)
else:
    sys.stderr.write("Encoding has to be either sparse or blosum.\n")
    sys.exit(2)

# get target length:
T_LEN = y_train[0].shape[0]

# always take max SH2 seq length, even if peptide sequence length is restricted:
MAX_SH2_SEQ_LEN = max(len(max(X_sh2_train, key=len)), len(max(X_sh2_val, key=len)))

if MAX_PEP_SEQ_LEN == -1:
    # no length restraint -> find max length in data set
    MAX_PEP_SEQ_LEN = max(len(max(X_pep_train, key=len)), len(max(X_pep_val, key=len)))
else:
    # remove peptides with length longer than max peptide length:
    idx = [i for i, x in enumerate(X_pep_train) if len(x) > MAX_PEP_SEQ_LEN]
    X_pep_train = [i for j, i in enumerate(X_pep_train) if j not in idx]
    X_sh2_train = [i for j, i in enumerate(X_sh2_train) if j not in idx]
    y_train = [i for j, i in enumerate(y_train) if j not in idx]

    idx = [i for i, x in enumerate(X_pep_val) if len(x) > MAX_PEP_SEQ_LEN]
    X_pep_val = [i for j, i in enumerate(X_pep_val) if j not in idx]
    X_sh2_val = [i for j, i in enumerate(X_sh2_val) if j not in idx]
    y_val = [i for j, i in enumerate(y_val) if j not in idx]


# save sequences as np.ndarray instead of list of np.ndarrays:
# for CNN we need pep + space + MHC and mask
X_pep_train = data_io_func.pad_seqs_T(X_pep_train, MAX_PEP_SEQ_LEN)
X_sh2_train = data_io_func.pad_seqs_T(X_sh2_train, MAX_SH2_SEQ_LEN)
X_pep_val = data_io_func.pad_seqs_T(X_pep_val, MAX_PEP_SEQ_LEN)
X_sh2_val = data_io_func.pad_seqs_T(X_sh2_val, MAX_SH2_SEQ_LEN)
assert X_pep_train.shape[1] == X_sh2_train.shape[1] == X_pep_val.shape[1] == X_sh2_val.shape[1]
N_FEATURES = X_pep_train.shape[1]
print("# N_FEATURES: " + str(N_FEATURES))

y_train = data_io_func.pad_seqs(y_train, T_LEN)
print("# y_train shape: " + str(y_train.shape))
y_val = data_io_func.pad_seqs(y_val, T_LEN)


# Prepare Theano variables for targets and learning rate:
sym_target = T.vector('targets', dtype='float32')
sym_l_rate = T.scalar()

# Build the network:
print("# Building the network...")

if ARCHITECTURE == "cnn":
    network, in_pep, in_sh2 = \
        NN_func.build_cnn(n_features=N_FEATURES, n_filters=N_FILTERS, filter_sizes=FILTER_SIZES, activation=ACTIVATION,
                          dropout_dense=DROPOUT_DENSE, dropout_cnn=DROPOUT_CNN, n_hid=N_HID, w_init=W_INIT, separate=SEPARATE,
                          merge=MERGE)

    architecture = np.array([ARCHITECTURE])
    FILTER_SIZES_STRING = str(FILTER_SIZES).strip('[]')
    net_hyper_params = np.array([N_FEATURES, N_FILTERS, FILTER_SIZES_STRING, ACTIVATION, DROPOUT_DENSE, DROPOUT_CNN, N_HID,
                                 W_INIT, SEPARATE, MERGE])
else:
    sys.stderr.write("Unknown architecture specified (option -architecture)!\n")
    sys.exit(2)

print("# params: " + str(lasagne.layers.get_all_params(network)))
weights = list(lasagne.layers.get_all_params(network))
weights = [str(e) for e in weights]
w = [i for i, j in enumerate(weights) if j == 'W' or j == 'W_in_to_ingate' or j == 'W_hid_to_ingate'
     or j == 'W_in_to_forgetgate' or j == 'W_hid_to_forgetgate' or j == 'W_in_to_cell' or j == 'W_hid_to_cell'
     or j == 'W_in_to_outgate' or j == 'W_hid_to_outgate']
print("# weights: " + str(w))
print("# number of layers: " + str(len(lasagne.layers.get_all_layers(network))))
print("# number of parameters: " + str(lasagne.layers.count_params(network)))

# INITIALIZE VARIABLES ---------------------------------------------------------

if CONTINUE:
    # get current parameters:
    params = lasagne.layers.get_all_param_values(network)
    old_params = np.load(modelfile)['arr_0']

    # check if dimensions match:
    assert len(old_params) == len(params)
    for j in range(0, len(old_params)):
        assert old_params[j].shape == params[j].shape
    # set parameters in network:
    lasagne.layers.set_all_param_values(network, old_params)

print("# Compiling Theano training and validation functions...")

# TRAINING FUNCTION -----------------------------------------------------------

prediction = lasagne.layers.get_output(network)

# loss function:
if COST_FUNCTION == "squared_error":
    loss = lasagne.objectives.squared_error(prediction.flatten(), sym_target)
else:
    sys.stderr.write("Unknown cost function specified (option -cost_function)!\n")
    sys.exit(2)
loss = loss.mean()

# update:
params = lasagne.layers.get_all_params(network, trainable=True)

# constrain gradients for CNN:
grads = T.grad(loss, params)
scaled_grads = lasagne.updates.total_norm_constraint(grads, max_norm=20)

if UPDATE == "sgd":
    updates = lasagne.updates.sgd(scaled_grads, params, learning_rate=sym_l_rate)
elif UPDATE == "rmsprop":
    updates = lasagne.updates.rmsprop(scaled_grads, params, learning_rate=sym_l_rate)
elif UPDATE == "adam":
    updates = lasagne.updates.adam(scaled_grads, params, learning_rate=sym_l_rate)
elif UPDATE == "adadelta":
    updates = lasagne.updates.adadelta(scaled_grads, params, learning_rate=sym_l_rate)
else:
    sys.stderr.write("Unknown update specified (option -update)!\n")
    sys.exit(2)

# compile training function
train_fn = theano.function([in_pep.input_var, in_sh2.input_var, sym_target, sym_l_rate], loss, updates=updates,
                           on_unused_input='warn')


# VALIDATION FUNCTION ----------------------------------------------------------
test_prediction = lasagne.layers.get_output(network, deterministic=True)

if COST_FUNCTION == "squared_error":
    test_loss = lasagne.objectives.squared_error(test_prediction.flatten(), sym_target)
else:
    sys.stderr.write("Unknown cost function (option -cost_function)!\n")
    sys.exit(2)
test_loss = test_loss.mean()


# compile validation function:
val_fn = theano.function([in_pep.input_var, in_sh2.input_var, sym_target], test_loss, on_unused_input='warn')

# TRAINING LOOP:----------------------------------------------------------------
start_time = time.time()

print("# Start training loop...")

if not NO_EARLY_STOP:
    b_epoch = 0
    b_train_err = 99
    b_val_err = 99

for e in EPOCHS:

    train_err = 0
    train_batches = 0
    val_err = 0
    val_batches = 0
    e_start_time = time.time()

    # shuffle training examples and iterate through minbatches:
    for batch in iterate_minibatches(X_pep_train, X_sh2_train, y_train, BATCH_SIZE):
        pep_sh2, pep_sh2_mask, targets = batch
        train_err += train_fn(pep_sh2, pep_sh2_mask, targets.flatten(), LEARNING_RATE)
        train_batches += 1

    # predict validation set:
    for batch in iterate_minibatches(X_pep_val, X_sh2_val, y_val, BATCH_SIZE):
        pep_sh2, pep_sh2_mask, targets = batch
        val_err += val_fn(pep_sh2, pep_sh2_mask, targets.flatten())
        val_batches += 1

    # save model:
    if NO_EARLY_STOP:
        np.savez(args.modelfile, lasagne.layers.get_all_param_values(network), architecture, net_hyper_params)
    else:
        # use early stopping, save only best model:
        if (val_err/val_batches) < b_val_err:
            np.savez(args.modelfile, lasagne.layers.get_all_param_values(network), architecture, net_hyper_params)
            b_val_err = val_err/val_batches
            b_train_err = train_err/train_batches
            b_epoch = e

    # print performance:
    print("Epoch " + str(e) + "\ttraining error: " + str(round(train_err/train_batches, 4)) +
          "\tvalidation error: " + str(round(val_err/val_batches, 4)) +
          "\ttime: " + str(round(time.time()-e_start_time, 3)) + " s")


# print best performance:
if not NO_EARLY_STOP:
    print("# Best epoch: " + str(b_epoch) + "\ttrain error: " + str(round(b_train_err, 4)) +
          "\tvalidation error: " + str(round(b_val_err, 4)))
# report total time used for training:
print("# Time for training: " + str(round((time.time()-start_time)/60, 3)) + " min")
print("# Done!")

