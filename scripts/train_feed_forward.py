#!/usr/bin/env python

"""
Feed forward net training using the Lasagne library:
https://github.com/Lasagne
"""

from __future__ import print_function
import argparse
import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T

import lasagne

import data_io_func
import NN_func

theano.config.floatX = 'float32'


##########################################################################
#  FUNCTIONS
##########################################################################


############################### Batch iterator ###############################
# This is just a simple helper function iterating over training data in
# mini-batches of a particular size, optionally in random order. It assumes
# data is available as numpy arrays. For big datasets, you could load numpy
# arrays as memory-mapped files (np.load(..., mmap_mode='r')), or write your
# own custom data iteration function. For small datasets, you can also copy
# them to GPU at once for slightly improved performance. This would involve
# several changes in the main program, though, and is not demonstrated here.


def iterate_minibatches(pep, targets, batchsize):
    assert pep.shape[0] == targets.shape[0]
    # shuffle:
    indices = np.arange(len(pep))
    np.random.shuffle(indices)
    for start_idx in range(0, len(pep) - batchsize + 1, batchsize):
        excerpt = indices[start_idx:start_idx + batchsize]
        yield pep[excerpt], targets[excerpt]


################################################################################
#  PARSE COMMANDLINE OPTIONS
################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-training_data', '--trainfile',  help="file with training data")
parser.add_argument('-validation_data', '--valfile',  help="file with validation data (for early stopping)")
parser.add_argument('-work_dir', '--working_directory',  help="directory to put each domain's work like its model files")
parser.add_argument('-model_out', '--modelfile',  help="file to store best model")
parser.add_argument('-print_out', '--printfile',  help="file to print output to")
parser.add_argument('-max_pep_seq_length', '--max_pep_seq_length',
                    help="maximal peptide sequence length, default = -1", default=-1)
parser.add_argument('-encoding', '--encoding',  help="encoding, default = sparse", default="sparse")
parser.add_argument('-blosum', '--blosum', help="file with BLOSUM matrix")
parser.add_argument('-batch_size', '--batch_size',  help="Mini batch size, default = 20", default=20)
parser.add_argument('-epochs', '--epochs',  help="Number of training epochs, default = 200", default=200)
parser.add_argument('-learning_rate', '--learning_rate',  help="Learning rate, default = 0.01", default=0.01)
parser.add_argument('-update', '--update',  help="Method for weight update, default = sgd", default="sgd")
parser.add_argument('-activation', '--activation',  help="Activation function, default = sigmoid", default="sigmoid")
parser.add_argument('-dropout', '--dropout',  help="Dropout (0-0.99 corresponding to 0-99%), default = 0", default=0)
parser.add_argument('-n_hid', '--n_hid',  help="Size of 1st hidden layer, default = 60", default=60)
parser.add_argument('-architecture', '--architecture',  help="Neural network architecture, default = feed_forward",
                    default="feed_forward")
parser.add_argument('-w_init', '--w_init',  help="Weight initialization, default = glorot_uniform",
                    default="glorot_uniform")
parser.add_argument('-cost_function', '--cost_function',  help="Cost function, default = squared_error",
                    default="squared_error")
parser.add_argument('-seed', '--seed',  help="Seed for random number init., default = -1", default=-1)
parser.add_argument('-no_early_stop', '--no_early_stop', action="store_true",
                    help="Turn off early stopping, default = False", default=False)
parser.add_argument('-continue_training', '--continue_training', action="store_true",
                    help="Continue training, default = False", default=False)
parser.add_argument('-domain', '--domain', help="A single domain to use, default = None meaning that all domains are used", default=None)
args = parser.parse_args()


# get training data file
if args.trainfile is not None:
    trainfile = args.trainfile
    print("# Training data file: " + args.trainfile)
else:
    sys.stderr.write("Please specify training data file!\n")
    sys.exit(2)

# get validation data file
if args.valfile is not None:
    valfile = args.valfile
    print("# Validation data file: " + args.valfile)
else:
    sys.stderr.write("Please specify validation data file!\n")
    sys.exit(2)

# get working directory
if args.working_directory is not None:
    work_dir = args.working_directory
else:
    sys.stderr.write("Please specify working directory!\n")
    sys.exit(2)

if args.modelfile is not None:
    modelfile = args.modelfile
else:
    sys.stderr.write("Please specify modelfile!\n")
    sys.exit(2)

if args.printfile is not None:
    printfile = args.printfile
else:
    sys.stderr.write("Please specify printfile!\n")
    sys.exit(2)

try:
    MAX_PEP_SEQ_LEN = int(args.max_pep_seq_length)
    print("# max. peptide sequence length: " + str(MAX_PEP_SEQ_LEN))
except argparse.ArgumentError:
    sys.stderr.write("Problem with max. peptide sequence length specification (option -max_pep_seq_length)!\n")
    sys.exit(2)

try:
    ENCODING = args.encoding
    print("# encoding: " + str(ENCODING))
except argparse.ArgumentError:
    sys.stderr.write("Problem with sequence encoding specification (option -encoding)!\n")
    sys.exit(2)

if ENCODING == "blosum":
    try:
        blosumfile = args.blosum
        print("# Blosum matrix file: " + str(blosumfile))
    except argparse.ArgumentError:
        sys.stderr.write("Blosum encoding requires blosum matrix file! (option -blosum)!\n")
        sys.exit(2)

try:
    BATCH_SIZE = int(args.batch_size)
    print("# batch size: " + str(BATCH_SIZE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with mini batch size specification (option -batch_size)!\n")
    sys.exit(2)

try:
    EPOCHS = range(1, int(args.epochs)+1)
    print("# number of training epochs: " + str(args.epochs))
except argparse.ArgumentError:
    sys.stderr.write("Problem with epochs specification (option -epochs)!\n")
    sys.exit(2)

try:
    LEARNING_RATE = float(args.learning_rate)
    print("# learning rate: " + str(LEARNING_RATE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with learning rate specification (option -learning_rate)!\n")
    sys.exit(2)

try:
    UPDATE = args.update
    print("# weight update method: " + str(UPDATE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with update specification (option -update)!\n")
    sys.exit(2)

try:
    ACTIVATION = args.activation
    print("# activation function: " + str(ACTIVATION))
except argparse.ArgumentError:
    sys.stderr.write("Problem with activation function specification (option -activation)!\n")
    sys.exit(2)

try:
    DROPOUT = float(args.dropout)
    print("# dropout: " + str(DROPOUT))
except argparse.ArgumentError:
    sys.stderr.write("Problem with dropout specification (option -dropout)!\n")
    sys.exit(2)

try:
    N_HID = int(args.n_hid)
    print("# number of hidden units: " + str(N_HID))
except argparse.ArgumentError:
    sys.stderr.write("Problem with number of hidden neurons specification (option -n_hid)!\n")
    sys.exit(2)

try:
    ARCHITECTURE = args.architecture
    print("# architecture: " + str(ARCHITECTURE))
except argparse.ArgumentError:
    sys.stderr.write("Problem with architecture specification (option -architecture)!\n")
    sys.exit(2)

try:
    W_INIT = args.w_init
    print("# w_init: " + str(W_INIT))
except argparse.ArgumentError:
    sys.stderr.write("Problem with weight initialization specification (option -w_init)!\n")
    sys.exit(2)

try:
    COST_FUNCTION = args.cost_function
    print("# cost_function: " + str(COST_FUNCTION))
except argparse.ArgumentError:
    sys.stderr.write("Problem with cost function specification (option -cost_function)!\n")
    sys.exit(2)

try:
    SEED = int(args.seed)
    print("# seed: " + str(SEED))
except argparse.ArgumentError:
    sys.stderr.write("Problem with seed specification (option -seed)!\n")
    sys.exit(2)

NO_EARLY_STOP = args.no_early_stop
if NO_EARLY_STOP:
    print("# Early stopping is turned OFF")
else:
    print("# Early stopping is turned ON")

CONTINUE = args.continue_training
if CONTINUE:
    print("# Training is CONTINUED")
else:
    print("# New training (NOT continuing old training)")

domains = [args.domain]

################################################################################
#  MAIN
################################################################################

# create list with names of all SH2 domains
if domains[0] is None:
    domains = data_io_func.get_sh2s(trainfile)

# train for each domain
for domain in domains:
    print("# Training on domain " + domain)
    # check if we are training on this domain
    if not os.path.isdir(work_dir + '/' + domain):
        print("# directory does not exist for domain " + domain + ", it is skipped.")
        continue

    # find full paths for modelfile and printfile
    modelpath = work_dir + '/' + domain + '/' + modelfile
    printpath = work_dir + '/' + domain + '/' + printfile
    # check if printing file exists and already shows training was completed before
    if os.path.isfile(printpath):
        printing = open(printpath, 'r')
        done = False
        for line in printing:
            if "Done!" in line:
                done = True
                print("# Domain " + domain + " is already trained. Skipping.")
                break
        printing.close()
        if done:
            continue

    # open print file for editing
    printing = open(printpath, 'w')
    # remove any leftover model file
    if os.path.isfile(modelpath):
        os.remove(modelpath)

    if SEED != -1:
        printing.write("# Setting seed for random number generation...\n")
        lasagne.random.set_rng(np.random.RandomState(seed=SEED))
        np.random.seed(seed=SEED)  # for shuffling training examples

    printing.write("# Loading data...\n")

    # encode data (list of numpy nd-arrays):
    if ENCODING == "sparse":
        X_train, y_train = data_io_func.enc_sparse_pep_sh2(trainfile, domain, None)
        X_val, y_val = data_io_func.enc_sparse_pep_sh2(valfile, domain, None)
    elif ENCODING == "blosum":
        blosum = data_io_func.read_blosum_MN(blosumfile)
        X_train, y_train = data_io_func.enc_blosum_pep_sh2(trainfile, domain, None, blosum)
        X_val, y_val = data_io_func.enc_blosum_pep_sh2(valfile, domain, None, blosum)
    else:
        sys.stderr.write("Encoding has to be either sparse or blosum.\n")
        sys.exit(2)

    # get target length:
    T_LEN = y_train[0].shape[0]

    if MAX_PEP_SEQ_LEN == -1:
        # no length restraint -> find max length in data set
        MAX_PEP_SEQ_LEN = max(len(max(X_train, key=len)), len(max(X_val, key=len)))
    else:
        # remove peptides with length longer than max peptide length:
        idx = [i for i, x in enumerate(X_train) if len(x) > MAX_PEP_SEQ_LEN]
        X_train = [i for j, i in enumerate(X_train) if j not in idx]
        y_train = [i for j, i in enumerate(y_train) if j not in idx]

        idx = [i for i, x in enumerate(X_val) if len(x) > MAX_PEP_SEQ_LEN]
        X_val = [i for j, i in enumerate(X_val) if j not in idx]
        y_val = [i for j, i in enumerate(y_val) if j not in idx]

    # save sequences as np.ndarray instead of list of np.ndarrays:
    X_train = data_io_func.pad_seqs(X_train, MAX_PEP_SEQ_LEN)
    N_FEATURES = X_train.shape[2]
    SEQ_LEN = X_train.shape[1]
    y_train = data_io_func.pad_seqs(y_train, T_LEN)
    X_val = data_io_func.pad_seqs(X_val, MAX_PEP_SEQ_LEN)
    y_val = data_io_func.pad_seqs(y_val, T_LEN)

    printing.write(str(X_train.shape) + '\n')
    printing.write(str(N_FEATURES) + '\n')
    printing.write(str(y_train.shape) + '\n')

    # Prepare Theano variables for targets and learning rate:
    sym_target = T.vector('targets', dtype='float32')
    sym_l_rate = T.scalar()

    # Build the network:
    printing.write("# Building the network...\n")

    if ARCHITECTURE == "feed_forward":
        network, in_pep = NN_func.build_feed_forward(n_features=N_FEATURES, seq_len=SEQ_LEN, activation=ACTIVATION,
                                                     dropout=DROPOUT, n_hid=N_HID, w_init=W_INIT)
        architecture = np.array([ARCHITECTURE])
        net_hyper_params = np.array([N_FEATURES, SEQ_LEN, ACTIVATION, DROPOUT, N_HID, W_INIT])

    else:
        sys.stderr.write("Unknown architecture specified (option -architecture)!\n")
        sys.exit(2)

    printing.write("# params: " + str(lasagne.layers.get_all_params(network)) + '\n')
    weights = list(lasagne.layers.get_all_params(network))
    weights = [str(e) for e in weights]
    w = [i for i, j in enumerate(weights) if j == 'W' or j == 'W_in_to_ingate' or j == 'W_hid_to_ingate'
         or j == 'W_in_to_forgetgate' or j == 'W_hid_to_forgetgate' or j == 'W_in_to_cell' or j == 'W_hid_to_cell'
         or j == 'W_in_to_outgate' or j == 'W_hid_to_outgate']
    printing.write("# weights: " + str(w) + '\n')
    printing.write("# number of layers: " + str(len(lasagne.layers.get_all_layers(network))) + '\n')
    printing.write("# number of parameters: " + str(lasagne.layers.count_params(network)) + '\n')

    # INITIALIZE VARIABLES ---------------------------------------------------------

    if CONTINUE:
        # get current parameters:
        params = lasagne.layers.get_all_param_values(network)
        old_params = np.load(modelpath)['arr_0']

        # check if dimensions match:
        assert len(old_params) == len(params)
        for j in range(0, len(old_params)):
            assert old_params[j].shape == params[j].shape
        # set parameters in network:
        lasagne.layers.set_all_param_values(network, old_params)

    printing.write("# Compiling Theano training and validation functions...\n")

    # TRAINING FUNCTION -----------------------------------------------------------

    prediction = lasagne.layers.get_output(network)

    # loss function:
    if COST_FUNCTION == "squared_error":
        loss = lasagne.objectives.squared_error(prediction.flatten(), sym_target)
    else:
        sys.stderr.write("Unknown cost function specified (option -cost_function)!\n")
        sys.exit(2)
    loss = loss.mean()

    # update:
    params = lasagne.layers.get_all_params(network, trainable=True)

    if UPDATE == "sgd":
        updates = lasagne.updates.sgd(loss, params, learning_rate=sym_l_rate)
    elif UPDATE == "rmsprop":
        updates = lasagne.updates.rmsprop(loss, params, learning_rate=sym_l_rate)
    elif UPDATE == "adam":
        updates = lasagne.updates.adam(loss, params, learning_rate=sym_l_rate)
    elif UPDATE == "adadelta":
        updates = lasagne.updates.adadelta(loss, params, learning_rate=sym_l_rate)
    else:
        sys.stderr.write("Unknown update specified (option -update)!\n")
        sys.exit(2)

    # compile training function
    train_fn = theano.function([in_pep.input_var, sym_target, sym_l_rate], loss, updates=updates)

    # VALIDATION FUNCTION ----------------------------------------------------------
    test_prediction = lasagne.layers.get_output(network, deterministic=True)

    if COST_FUNCTION == "squared_error":
        test_loss = lasagne.objectives.squared_error(test_prediction.flatten(),sym_target)
    test_loss = test_loss.mean()


    # compile validation function:
    val_fn = theano.function([in_pep.input_var, sym_target], test_loss)

    # TRAINING LOOP:----------------------------------------------------------------
    start_time = time.time()

    printing.write("# Start training loop...\n")

    if not NO_EARLY_STOP:
        b_epoch = 0
        b_train_err = 99
        b_val_err = 99

    for e in EPOCHS:

        train_err = 0
        train_batches = 0
        val_err = 0
        val_batches = 0
        e_start_time = time.time()

        # shuffle training examples and iterate through minbatches:
        for batch in iterate_minibatches(X_train, y_train, BATCH_SIZE):
            pep, targets = batch
            train_err += train_fn(pep, targets.flatten(), LEARNING_RATE)
            train_batches += 1

        # predict validation set:
        for batch in iterate_minibatches(X_val, y_val, BATCH_SIZE):
            pep, targets = batch
            val_err += val_fn(pep, targets.flatten())
            val_batches += 1

        # save model:
        if NO_EARLY_STOP:
            np.savez(modelpath, lasagne.layers.get_all_param_values(network), architecture, net_hyper_params)
        else:
            # use early stopping, save only best model:
            if (val_err/val_batches) < b_val_err:
                np.savez(modelpath, lasagne.layers.get_all_param_values(network), architecture, net_hyper_params)
                b_val_err = val_err/val_batches
                b_train_err = train_err/train_batches
                b_epoch = e

        # print performance:
        printing.write("Epoch " + str(e) + "\ttraining error: " + str(round(train_err/train_batches, 4)) +
                       "\tvalidation error: " + str(round(val_err/val_batches, 4)) + "\ttime: " +
                       str(round(time.time() - e_start_time, 3)) + " s\n")

    # print best performance:
    if not NO_EARLY_STOP:
        printing.write("# Best epoch: " + str(b_epoch) + "\ttrain error: " + str(round(b_train_err, 4)) +
                       "\tvalidation error: " + str(round(b_val_err, 4)) + '\n')
    # report total time used for training:
    printing.write("# Time for training: " + str(round((time.time()-start_time)/60, 3)) + " min\n")
    printing.write("# Done!\n")
    printing.close()

print("# Done!\n")
