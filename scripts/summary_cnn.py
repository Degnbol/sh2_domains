#!/usr/bin/env python

"""
Calculates the Pearson Correlation Coefficient for each combination of hyper parameters to find to
find the best combination. They are collected in a single file that can afterwards be imported into excel to see any
patterns.
"""

import argparse
import sys
import os
import numpy as np
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from sklearn.metrics import roc_auc_score

################################################################################
#  PARSE COMMANDLINE OPTIONS
################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-prediction_file', '--prediction_file',
                    help="File containing all peptides, their prediction and target value")
parser.add_argument('-out', '--outfile',  help="File to store output table")
parser.add_argument('-nested', '--nested', help="Whether the training was nested cross-validation or not")
parser.add_argument('-update', '--update', help="Weight update function used")
parser.add_argument('-activation', '--activation', help="Activation function used in each node")
parser.add_argument('-data_enc', '--data_encoding', help="Encoding of peptides with either sparse or BLOSUM")
parser.add_argument('-dropout_dense', '--dropout_dense',  help="Dropout after dense layer (0-0.99 corresponding to 0-99%)")
parser.add_argument('-dropout_cnn', '--dropout_cnn',  help="Dropout after CNN layers (0-0.99 corresponding to 0-99%)")
parser.add_argument('-lr', '--learning_rate', help="Learning rate used for the models")
parser.add_argument('-n_hid', '--hidden_nodes', help="Number of hidden nodes used")
parser.add_argument('-n_filters', '--n_filters', help="Number of filters for each CNN layer")
parser.add_argument('-filter_sizes', '--filter_sizes', nargs="+", help="Size of the filters in the CNN, default=[3,5,7]",
                    default=[3, 5, 7])
parser.add_argument('-transform', '--transformation',
                    help="If the transformation used is specified, the values are transformed back to find correlations")
parser.add_argument('-b', '--beta', help="For a sigmoid transformation a beta value can be specified, default=1",
                    default=1)
# calling this argument separate is mostly to keep the same name as used for LSTM.
# The peptide and SH2 domain are kept separate in both cases before concatenation, but if separate is true,
# the peptide is not run through CNN layers.
parser.add_argument('-separate', '--separate',
                    help="Run convolutions on SH2 domain instead of also running convolutions on peptides, a string yes or no")
parser.add_argument('-loo', '--leaveoneout',
                    help="Domain left out in leave-one-out experiment. None is normal answer")
parser.add_argument('-merge', '--merge',
                    help="Merged peptide and SH2 as the first thing, making them share CNN layers, a string yes or no")
args = parser.parse_args()

if args.prediction_file is not None:
    prediction_file = args.prediction_file
else:
    sys.stderr.write("Please specify prediction file!\n")
    sys.exit(2)

# get outfile:
if args.outfile is not None:
    outfile = args.outfile
else:
    sys.stderr.write("Please specify output file!\n")
    sys.exit(2)

if args.nested is not None:
    nested = args.nested
else:
    sys.stderr.write("Please specify nested!\n")
    sys.exit(2)

if args.update is not None:
    update = args.update
else:
    sys.stderr.write("Please specify update function!\n")
    sys.exit(2)

if args.activation is not None:
    activation = args.activation
else:
    sys.stderr.write("Please specify activation function!\n")
    sys.exit(2)

if args.data_encoding is not None:
    data_encoding = args.data_encoding
else:
    sys.stderr.write("Please specify data encoding!\n")
    sys.exit(2)

if args.dropout_dense is not None:
    dropout_dense = args.dropout_dense
else:
    sys.stderr.write("Please specify dropout dense!\n")
    sys.exit(2)

if args.dropout_cnn is not None:
    dropout_cnn = args.dropout_cnn
else:
    sys.stderr.write("Please specify dropout cnn!\n")
    sys.exit(2)

if args.learning_rate is not None:
    learning_rate = args.learning_rate
else:
    sys.stderr.write("Please specify learning rate!\n")
    sys.exit(2)

if args.hidden_nodes is not None:
    n_hid = args.hidden_nodes
else:
    sys.stderr.write("Please specify number of hidden nodes!\n")
    sys.exit(2)

if args.n_filters is not None:
    n_filters = args.n_filters
else:
    sys.stderr.write("Please specify number of filters per CNN layer!\n")
    sys.exit(2)

if args.filter_sizes is not None:
    filter_sizes = args.filter_sizes
else:
    sys.stderr.write("Please specify filter sizes!\n")
    sys.exit(2)

if args.separate is not None:
    separate = args.separate
else:
    sys.stderr.write("Please specify separate!\n")
    sys.exit(2)

if args.leaveoneout is not None:
    loo = args.leaveoneout
else:
    sys.stderr.write("Please specify loo!\n")
    sys.exit(2)

if args.merge is not None:
    merge = args.merge
else:
    sys.stderr.write("Please specify merge!\n")
    sys.exit(2)

# get transformation and possibly beta
if args.transformation is not None:
    if args.transformation == 'sigmoid':
        transform = args.transformation
        if args.beta is not None:
            try:
                beta = float(args.beta)
            except argparse.ArgumentError:
                sys.stderr.write("Problem with beta value!\n")
                sys.exit(2)
        else:
            # this code should never be reachable since there is specified a default value in argparse
            beta = 1
    else:
        sys.stderr.write("Unknown transformation specified!\n")
        sys.exit(2)
else:
    transform = None


# define inverse functions
def inverse_sigmoid(target, beta):

    zscore = target.copy()
    # since it is a piecewise function
    zscore[target <= 0] = 0
    zscore[(target > 0) & (target < 1)] = -np.log(1 / zscore[(target > 0) & (target < 1)] - 1) * beta + 2
    zscore[target >= 1] = max(zscore[(target > 0) & (target < 1)])

    return zscore


################################################################################
#  MAIN
################################################################################

# open file
with open(outfile, 'w') as out:
    # write header
    out.write("SH2\tPCC\tspearman\tAUC\tupdate\tactivation\tdata_encoding\tdropout_dense\tdropout_cnn\tlearning_rate\thidden_nodes\tn_filters\tfilter_sizes\tseparate\tnested\tloo\tmerge\n")

    # collect data from file
    domains = []
    pred = []
    y = []
    with open(prediction_file, 'r') as predfile:
        # skip header
        next(predfile)

        for line in predfile:
            l = line.strip().split()
            domains.append(l[1])
            pred.append(float(l[2]))
            y.append(float(l[3]))

    domains = np.array(domains)
    pred = np.array(pred)
    y = np.array(y)
    assert len(domains) == pred.shape[0] == y.shape[0]

    if transform is not None:
        print("# Transforming prediction and target values back to z-scores.")

        if transform == 'sigmoid':
            print("# using the inverse of sigmoid transformation with beta=" + str(beta) + ".")
            pred = inverse_sigmoid(pred, beta)
            y = inverse_sigmoid(y, beta)

    # unique domains
    uniques = list(set(domains))
    print("# Number of unique domains: " + str(len(uniques)))
    for unique in uniques:

        index = unique == domains
        pred_sub = pred[index]
        y_sub = y[index]

        print("# Calculate PCC, spearman and AUC.")
        # PCC
        pcc, pval = pearsonr(pred_sub.flatten(), y_sub.flatten())
        # spearman
        spear, pval2 = spearmanr(pred_sub.flatten(), y_sub.flatten())
        # AUC
        y_binary = np.where(y_sub >= 0.5, 1, 0)
        auc = roc_auc_score(y_binary.flatten(), pred_sub.flatten())

        # save to file
        out.write(
            unique + '\t' + str(pcc) + '\t' + str(spear) + '\t' + str(auc) + '\t' + update + '\t' + activation + '\t' +
            data_encoding + '\t' + dropout_dense + '\t' + dropout_cnn + '\t' + learning_rate + '\t' + n_hid + '\t' +
            n_filters + '\t' + str(np.array(filter_sizes, dtype=int)).strip('[]') + '\t' + separate + '\t' + nested +
            '\t' + loo + '\t' + merge + '\n')

print("# Done!")





