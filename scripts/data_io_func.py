#!/usr/bin/env python

"""
Functions for data IO for neural network training.
"""

from __future__ import print_function
import argparse
import sys
import os
import time
import csv

from operator import add
import math
import numpy as np
from scipy.io import netcdf
import theano
import theano.tensor as T

import lasagne

theano.config.floatX = 'float32'


def get_sh2s(filename):
    '''
    return a list of the SH2 domains found in a file with peptide AA sequences

    parameters:
        - filename : file with AA seq of peptide, target and name of SH2 domain
    returns:
        - sh2s : list with names of the SH2 domains
    '''

    sh2s = []

    infile = open(filename, "r")
    for l in infile:
        l = filter(None, l.strip().split())

        if l[2] in sh2s:
            break  # assumes that all sh2s are present in the first rows and then repeats again and again

        sh2s.append(l[2])

    # close input file:
    infile.close()

    # return data:
    return sh2s


def read_sh2_seq(filename):
    '''
    read in SH2 sequences

    parameters:
        - filename : file containing SH2 sequences

    returns:
        - sh2 : dictionary sh2 -> AA sequence (as string)
    '''
    # read SH2 sequence:
    file = open(filename, 'r')
    content = file.read().splitlines()

    # make dictionary
    sh2 = {}

    for line in content:
        l = line.split()
        sh2[l[0]] = l[1]

    file.close()

    return sh2


def enc_sparse_pep_sh2(filename, sh2_dict, sh2_ignore):
    '''
    sparse encoding of peptide, save target values

    parameters:
        - filename : file with AA seq of peptide, target and name of SH2 domain
        - sh2_dict : name of the current SH2 domain to look for in the file (only used for feed-forward) or a dictionary
         of all SH2s to look for. Can also be None to indicate that domains are already written as amino acids
        - sh2_ignore : sh2 domain to ignore in leave-one-out training. use None to not use this parameter
    returns:
        - peptides : list of np.ndarrays containing encoded peptide sequences
        - targets : list of np.ndarrays containing targets (log transformed IC 50 values)
    '''

    # create a boolean to tell whether we are encoding SH2 domain sequences or simply using a single domain name to
    # check if we should include the peptide or not
    is_pan_specific = isinstance(sh2_dict, dict) or sh2_dict is None

    # define sparse AA alphabet:
    alphabet = ["A","R","N","D","C","Q","E","G","H","I","L","K","M","F","P","S","T","W","Y","V"]
    sparse = {}
    count = 0
    for aa in alphabet:
        sparse[aa] = np.ones(20)*0.05
        sparse[aa][count] = 0.9
        count += 1
    sparse["X"] = np.ones(20)*0.05

    # read and encode data:
    peptides = []
    sh2s = []
    targets = []

    infile = open(filename, "r")
    for l in infile:
        l = filter(None, l.strip().split())
        pepseq = l[0]
        domain = l[2]

        # check if we should skip domain
        # skip the sh2_ignore
        if sh2_ignore is not None and domain == sh2_ignore:
            continue
        if sh2_dict is not None:
            if is_pan_specific:
                # skip domain if not found in dictionary
                if domain not in sh2_dict:
                    continue
            else:
                # skip domain if it is not the specified one to use in the variable sh2_dict
                if domain != sh2_dict:
                    continue

        # encode peptide sequence:
        pep = np.zeros((len(pepseq), 20))
        count = 0
        for aa in pepseq:
            if aa in sparse:
                pep[count] = sparse[aa]
                count += 1
            else:
                sys.stderr.write("Unknown amino acid in peptides: " + aa + ", encoding aborted!\n")
                sys.exit(2)
        peptides.append(pep.astype(theano.config.floatX))

        # encode SH2 sequence:
        if is_pan_specific:
            # find SH2 sequence from dictionary or from the file directly
            if sh2_dict is None:
                sh2_sequence = domain
            else:
                sh2_sequence = sh2_dict[domain]

            sh2 = np.zeros((len(sh2_sequence), 20))
            count = 0
            for aa in sh2_sequence:
                if aa in sparse:
                    sh2[count] = sparse[aa]
                    count += 1
                else:
                    sys.stderr.write("Unknown amino acid in SH2: " + aa + ", encoding aborted!\n")
                    sys.exit(2)
            sh2s.append(sh2.astype(theano.config.floatX))

        try:
            # save target value:
            target = np.ones((1, 1)) * float(l[1])
            targets.append(target.astype(theano.config.floatX))
        except:
            tmp=1
            #sys.stderr.write("No target values "+ filename +"!\n")

    # close input file:
    infile.close()

    # return data:
    if is_pan_specific:
        return peptides, sh2s, targets
    else:
        return peptides, targets


def enc_blosum_pep_sh2(filename, sh2_dict, sh2_ignore, blosum):
    '''
    blosum encoding of peptide and SH2, save target values

    parameters:
        - filename : file with AA seq of peptide, target and name of SH2 domains
        - sh2_dict : name of the current SH2 domain to look for in the file (only used for feed-forward) or a dictionary
         of all SH2s to look for. Can also be None to indicate that domains are already written as amino acids
        - sh2_ignore : sh2 domain to ignore in leave-one-out training. use None to not use this parameter
        - blosum : dictionnary: key= AA, value= blosum encoding
    returns:
        - peptides : list of np.ndarrays containing encoded peptide sequences
        - targets : list of np.ndarrays containing targets (log transformed IC 50 values)
    '''

    # create a boolean to tell whether we are encoding SH2 domain sequences or simply using a single domain name to
    # check if we should include the peptide or not
    is_pan_specific = isinstance(sh2_dict, dict) or sh2_dict is None

    # read and encode data:
    peptides = []
    sh2s = []
    targets = []

    infile = open(filename, "r")
    for l in infile:
        l = filter(None, l.strip().split())
        pepseq = l[0]
        domain = l[2]

        # check if we should skip domain
        # skip the sh2_ignore
        if sh2_ignore is not None and domain == sh2_ignore:
            continue
        if sh2_dict is not None:
            if is_pan_specific:
                # skip domain if not found in dictionary
                if domain not in sh2_dict:
                    continue
            else:
                # skip domain if it is not the specified one to use in the variable sh2_dict
                if domain != sh2_dict:
                    continue

        # encode peptide sequence:
        pep = np.zeros((len(pepseq), len(blosum["A"])))
        count = 0
        for aa in pepseq:
            if aa in blosum:
                pep[count] = blosum[aa]
                count += 1
            else:
                sys.stderr.write("Unknown amino acid in peptides: " + aa + ", encoding aborted!\n")
                sys.exit(2)
        peptides.append(pep.astype(theano.config.floatX))

        # encode SH2 sequence:
        if is_pan_specific:
            # find SH2 sequence from dictionary or from the file directly
            if sh2_dict is None:
                sh2_sequence = domain
            else:
                sh2_sequence = sh2_dict[domain]

            sh2 = np.zeros((len(sh2_sequence), len(blosum["A"])))
            count = 0
            for aa in sh2_sequence:
                if aa in blosum:
                    sh2[count] = blosum[aa]
                    count += 1
                else:
                    sys.stderr.write("Unknown amino acid in SH2: " + aa + ", encoding aborted!\n")
                    sys.exit(2)
            sh2s.append(sh2.astype(theano.config.floatX))

        # save target values:
        try:
            # save target value:
            target = np.ones((1, 1)) * float(l[1])
            targets.append(target.astype(theano.config.floatX))
        except:
            tmp=1
            #sys.stderr.write("No target values "+ filename +"!\n")

    # close input file:
    infile.close()

    # return data:
    if is_pan_specific:
        return peptides, sh2s, targets
    else:
        return peptides, targets



def read_blosum_MN(filename):
    '''
    read in BLOSUM matrix

    parameters:
        - filename : file containing BLOSUM matrix

    returns:
        - blosum : dictionnary AA -> blosum encoding (as list)
    '''

    # read BLOSUM matrix:
    blosumfile = open(filename, "r")
    blosum = {}
    B_idx = 99
    Z_idx = 99
    star_idx = 99

    for l in blosumfile:
        l = l.strip()

        if l[0] != '#':
            l = filter(None, l.strip().split(" "))

            if (l[0] == 'A') and (B_idx == 99):
                B_idx = l.index('B')
                Z_idx = l.index('Z')
                star_idx = l.index('*')
            else:
                aa = str(l[0])
                if (aa != 'B') &  (aa != 'Z') & (aa != '*'):
                    tmp = l[1:len(l)]
                    # tmp = [float(i) for i in tmp]
                    # get rid of BJZ*:
                    tmp2 = []
                    for i in range(0, len(tmp)):
                        if (i != B_idx) &  (i != Z_idx) & (i != star_idx):
                            tmp2.append(round(0.2*float(tmp[i]), 2))  # divide by five

                    #save in BLOSUM matrix
                    #[i * 0.2 for i in tmp2] #scale (divide by 5)
                    blosum[aa] = tmp2
    blosumfile.close()
    return blosum


# modified from nntools:--------------------------------------------------------
def pad_seqs(X, length):
    '''
    Convert a list of matrices into np.ndarray

    parameters:
        - X : list of np.ndarray
            List of matrices
        - length : int
            Desired sequence length.  Smaller sequences will be padded with 0s,
            longer will be truncated.
        - batch_size : int
            Mini-batch size

    returns:
        - X_batch : np.ndarray
            Tensor of time series matrix batches,
            shape=(n_batches, length, n_features)
    '''

    n_seqs = len(X)
    n_features = X[0].shape[1]

    X_pad = np.zeros((n_seqs, length, n_features), dtype=theano.config.floatX)
    for i in range(0, len(X)):
        X_pad[i, :X[i].shape[0], :n_features] = X[i]
    return X_pad


def get_pep_aa_sh2(filename, sh2_dict, MAX_PEP_SEQ_LEN):
    '''
    read AA seq of peptides and SH2 domain from text file

    parameters:
        - filename : file in which data is stored
        - sh2_dict : name of the current SH2 domain to look for in the file or a dictionary of all SH2s to look for.
        can also be None to indicate that SH2 domains in the file are written as amino acids and not as names to look for
        in a dictionary.
    returns:
        - pep_aa : list of amino acid sequences of peptides (as string)
        - sh2s : name of current SH2 domain
    '''

    # create a boolean to tell whether we are encoding SH2 domain sequences or simply using a single domain name to
    # check if we should include the peptide or not
    is_pan_specific = isinstance(sh2_dict, dict) or sh2_dict is None

    pep_aa = []
    sh2s = []
    infile = open(filename, "r")

    for l in infile:
        l = filter(None, l.strip().split())
        peptide = l[0]
        domain = l[2]

        if sh2_dict is not None:
            if is_pan_specific:
                # only add peptide and target values for SH2 domains specified which are found in the dictionary
                if domain not in sh2_dict:
                    continue
            else:
                # only add peptide and target values for the rows for this specific SH2 domain
                if domain != sh2_dict:
                    continue

        if len(l[0]) <= MAX_PEP_SEQ_LEN:
            pep_aa.append(peptide)
            sh2s.append(domain)
    infile.close()

    if is_pan_specific:
        return pep_aa, sh2s
    else:
        return pep_aa



def pad_pep_sh2_mask_multi(X_pep, X_sh2, max_pep_seq_len, max_sh2_seq_len, n_aa):
    '''
    Convert a list of matrices into np.ndarray

    parameters:
        - X_pep : list of np.ndarray
            List of matrices containing encoded peptide sequences
        - X_sh2 : list of np.ndarray
            List of matrices containing encoded SH2 sequences
        - max_pep_seq_len : int
            Sequence length of peptides. Smaller sequences will be padded with 0s, longer will be truncated.
        - max_sh2_seq_len : int
            Sequence length of SH2. Smaller sequences will be padded with 0s, longer will be truncated.
        -n_aa: number of AA to present in each time step.

    returns:
        - X_pad : np.ndarray
            Tensor of time series matrix batches, shape=(n_seq, time_steps, n_features_new)
        - X_mask : np.ndarray
            Tensor denoting what to include, shape=(n_batches, batch_size, length, n_features)
            separate encoding of peptide and MHC sequence + peptide length encoding
    '''

    assert(len(X_pep) == len(X_sh2))
    n_seqs = len(X_pep)
    n_features = X_pep[0].shape[1]
    time_steps = int(math.ceil(max_sh2_seq_len / float(n_aa)) + math.ceil(max_pep_seq_len / float(n_aa)) + 1)
    ts_sh2 = int(math.ceil(max_sh2_seq_len / float(n_aa)))
    # ts_pep = int(math.ceil(max_pep_seq_len / float(n_aa)))
    X_pad = np.zeros((n_seqs, time_steps, (2 * n_aa * n_features)), dtype=theano.config.floatX)
    X_mask = np.zeros((n_seqs, time_steps), dtype=np.bool)

    for i in range(0, n_seqs):
        # SH2 sequence:
        c = 0
        for j in range(0, ts_sh2):
            if c < X_sh2[i].shape[0]:
                tmp = X_sh2[i][c:min(c+n_aa, time_steps)].flatten()
                X_pad[i, j, :len(tmp)] = tmp
                X_mask[i, j] = 1

            c += n_aa

        # spacer between SH2 and peptide:
        X_pad[i, ts_sh2, :(2*n_aa*n_features)] = 1
        X_mask[i, ts_sh2] = 1

        # peptide sequence:
        c = 0
        for j in range((ts_sh2 + 1), time_steps):
            if c < X_pep[i].shape[0]:
                tmp = X_pep[i][c:min(c+n_aa, time_steps)].flatten()
                X_pad[i, j, (n_aa*n_features):(n_aa*n_features+len(tmp))] = tmp
                X_mask[i, j] = 1

            c += n_aa

    return X_pad, X_mask


def pad_pep_sh2_mask_multi_separate(X_pep, X_sh2, max_pep_seq_len, max_sh2_seq_len, n_aa):
    '''
    Convert a list of matrices into np.ndarray

    parameters:
        - X_pep : list of np.ndarray
            List of matrices containing encoded peptide sequences
        - X_sh2 : list of np.ndarray
            List of matrices containing encoded SH2 sequences
        - max_pep_seq_len : int
            Sequence length of peptides. Smaller sequences will be padded with 0s, longer will be truncated.
        - max_sh2_seq_len : int
            Sequence length of SH2. Smaller sequences will be padded with 0s, longer will be truncated.
        -n_aa: number of AA to present in each time step.

    returns:
        - X_pep_pad : np.ndarray
            Tensor of time series matrix batches
        - X_sh2_pad : np.ndarray
            Tensor of time series matrix batches
        - X_mask : np.ndarray
            Tensor denoting what to include
    '''

    assert(len(X_pep) == len(X_sh2))
    n_seqs = len(X_pep)
    n_features = X_pep[0].shape[1]
    ts_sh2 = int(math.ceil(max_sh2_seq_len / float(n_aa)))
    X_pep_pad = np.zeros((n_seqs, max_pep_seq_len, n_features), dtype=theano.config.floatX)
    X_sh2_pad = np.zeros((n_seqs, ts_sh2, n_aa * n_features), dtype=theano.config.floatX)
    X_mask = np.zeros((n_seqs, ts_sh2), dtype=np.bool)

    for i in range(0, n_seqs):

        # peptide sequence:
        for j in range(0, max_pep_seq_len):
            X_pep_pad[i, j, :] = X_pep[i][j].flatten()

        # SH2 sequence:
        c = 0
        for j in range(0, ts_sh2):
            if c < X_sh2[i].shape[0]:
                tmp = X_sh2[i][c:min(c + n_aa, ts_sh2)].flatten()
                X_sh2_pad[i, j, :len(tmp)] = tmp
                X_mask[i, j] = 1

            c += n_aa

    return X_pep_pad, X_sh2_pad, X_mask


def pad_seqs_T(X, length):
    '''
    Convert a list of matrices into np.ndarray

    parameters:
        - X : list of np.ndarray
            List of matrices
        - length : int
            Desired sequence length.  Smaller sequences will be padded with 0s,
            longer will be truncated.

    returns:
        - X_batch : np.ndarray
            Tensor of time series matrix batches,
            shape=(n_batches, n_features, seqlength)
    '''

    n_seqs = len(X)
    n_features = X[0].shape[1]

    X_pad = np.zeros((n_seqs, n_features, length), dtype=theano.config.floatX)
    for i in range(0, len(X)):
        slen = X[i].shape[0]
        X_pad[i, :n_features, :slen] = np.swapaxes(X[i], 0, 1)
    return X_pad


