#!/usr/bin/env python

"""
Predict data with feed forward net trained using the Lasagne library:
https://github.com/Lasagne
"""

from __future__ import print_function
import argparse
import sys
import os
import time

import csv
import numpy as np
from scipy.io import netcdf
from scipy.stats import pearsonr
from sklearn.metrics import roc_auc_score
import theano
import theano.tensor as T

import lasagne

import data_io_func
import NN_func


##########################################################################
#	FUNCTIONS
##########################################################################


############################### Batch iterator ###############################
# This is just a simple helper function iterating over training data in
# mini-batches of a particular size, optionally in random order. It assumes
# data is available as numpy arrays. For big datasets, you could load numpy
# arrays as memory-mapped files (np.load(..., mmap_mode='r')), or write your
# own custom data iteration function. For small datasets, you can also copy
# them to GPU at once for slightly improved performance. This would involve
# several changes in the main program, though, and is not demonstrated here.
def iterate_minibatches(pep, sh2, batchsize):
    assert pep.shape[0] == sh2.shape[0]
    indices = np.arange(len(pep))
    for start_idx in range(0, len(pep), batchsize):
        excerpt = indices[start_idx:start_idx + batchsize]
        yield pep[excerpt], sh2[excerpt], excerpt


################################################################################
#	PARSE COMMANDLINE OPTIONS
################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-test_data', '--testfile',  help="file with data to be predicted")
parser.add_argument('-domain_file', '--domain_file',  help="file with SH2 domain sequences")
parser.add_argument('-encoding', '--encoding',  help="encoding, default = sparse", default="sparse")
parser.add_argument('-blosum', '--blosum', help="file with BLOSUM matrix")
parser.add_argument('-max_pep_seq_length', '--max_pep_seq_length',  help="max peptide sequence length, default = 20",
                    default=20)
parser.add_argument('-out', '--outfile',  help="file to store output table")
parser.add_argument('-ensemblelist', '--ensemblelist', nargs='+', help="list of files that are the ensemble models")
# calling this argument separate is mostly to keep the same name as used for LSTM.
# The peptide and SH2 domain are kept separate in both cases before concatenation, but if separate is true,
# the peptide is not run through CNN layers.
parser.add_argument('-separate', '--separate', action="store_true",
                    help="Run convolutions on SH2 domain instead of also running convolutions on peptides, default = False", default=False)
parser.add_argument('-merge', '--merge', action="store_true",
                    help="Concatenate peptide and SH2 from the start, making them share CNN layers, default = False", default=False)
parser.add_argument('-loo', '--leaveoneout', help="SH2 domain name for domain to leave out of training.", default=None)
args = parser.parse_args()

# get testfile:
if args.testfile is not None:
    testfile = args.testfile
else:
    sys.stderr.write("Please specify testfile!\n")
    sys.exit(2)

# get domain file
domain_file = args.domain_file
if domain_file is not None:
    print("# Domain file: " + args.domain_file)
else:
    sys.stdout.write("Domain file not specified. Assuming all domains are written as amino acids.\n")

try:
    ENCODING = args.encoding
    print("# encoding: " + str(ENCODING))
except argparse.ArgumentError:
    sys.stderr.write("Problem with sequence encoding specification (option -encoding)!\n")
    sys.exit(2)

if ENCODING == "blosum":
    try:
        blosumfile = args.blosum
        print("# Blosum matrix file: " + str(blosumfile))
    except argparse.ArgumentError:
        sys.stderr.write("Blosum encoding requires blosum matrix file! (option -blosum)!\n")
        sys.exit(2)

try:
    MAX_PEP_SEQ_LEN = int(args.max_pep_seq_length)
except argparse.ArgumentError:
    sys.stderr.write("Problem with max. peptide sequence length specification (option -max_pep_seq_length)!\n")
    sys.exit(2)

# get outputfile:
if args.outfile is not None:
    outfilename = args.outfile
else:
    sys.stderr.write("Please specify output file!\n")
    sys.exit(2)

# get ensemble list:
if args.ensemblelist is not None:
    ensemble = args.ensemblelist
else:
    sys.stderr.write("Please specify data file with hyper parameters and weight files!\n")
    sys.exit(2)

SEPARATE = args.separate
if SEPARATE:
    print("# Peptide is encoded and NOT run through CNN layers before being concatenated with SH2 domain.")
else:
    print("# Peptide is run through CNN layers just like SH2 domain before they are concatenated.")

MERGE = args.merge
if MERGE:
    print("# Peptide and SH2 are concatenated as the first thing and share CNN layers.")
else:
    print("# Peptide and SH2 are not concatenated as the first thing. Argument separate will determine what is done.")

LEAVE_ONE_OUT = args.leaveoneout
print("# Leave-one-out SH2 domain: " + args.leaveoneout)


# constant set for batch size. This is because matrices can get too big for the network to handle in one go.
BATCH_SIZE = 10000

################################################################################
#   LOAD DATA
################################################################################

print("# Loading data...")

if domain_file is None:
    # there will be made no dictionary for SH2 domains
    sh2_seq = None
else:
    # make dictionary of sh2 domains and their AA sequences
    sh2_seq = data_io_func.read_sh2_seq(domain_file)
    if LEAVE_ONE_OUT not in sh2_seq:
        print("# Leave-one-out SH2 domain not found in list of SH2 domains so normal prediction is performed.")
    else:
        sh2_seq = {LEAVE_ONE_OUT: sh2_seq[LEAVE_ONE_OUT]}
        print("# Leave-one-out SH2 domain was found in list of SH2 domains so prediction is only performed on this domain.")

# encode data (list of numpy nd-arrays):
if ENCODING == "sparse":
    X_pep, X_sh2, y = data_io_func.enc_sparse_pep_sh2(testfile, sh2_seq, None)
elif ENCODING == "blosum":
    blosum = data_io_func.read_blosum_MN(blosumfile)
    X_pep, X_sh2, y = data_io_func.enc_blosum_pep_sh2(testfile, sh2_seq, None, blosum)
else:
    sys.stderr.write("Encoding has to be either sparse or blosum.\n")
    sys.exit(2)

# get target length:
T_LEN = y[0].shape[0]

# always take max SH2 seq length, even if peptide sequence length is restricted:
MAX_SH2_SEQ_LEN = len(max(X_sh2, key=len))

if MAX_PEP_SEQ_LEN == -1:
    # no length restraint -> find max length in data set
    MAX_PEP_SEQ_LEN = len(max(X_pep, key=len))
else:
    # remove peptides with length longer than max peptide length:
    idx = [i for i, x in enumerate(X_pep) if len(x) > MAX_PEP_SEQ_LEN]
    X_pep = [i for j, i in enumerate(X_pep) if j not in idx]
    X_sh2 = [i for j, i in enumerate(X_sh2) if j not in idx]
    y = [i for j, i in enumerate(y) if j not in idx]

n_pep = len(X_pep)
print("# len X pep: " + str(n_pep))

# save sequences as np.ndarray instead of list of np.ndarrays:
# for CNN we need pep + space + MHC and mask
X_pep = data_io_func.pad_seqs_T(X_pep, MAX_PEP_SEQ_LEN)
X_sh2 = data_io_func.pad_seqs_T(X_sh2, MAX_SH2_SEQ_LEN)
print("# X_pep shape: " + str(X_pep.shape))
print("# X_sh2 shape: " + str(X_sh2.shape))
assert X_pep.shape[1] == X_sh2.shape[1]
N_FEATURES = X_pep.shape[1]
print("# N_FEATURES: " + str(N_FEATURES))
y = data_io_func.pad_seqs(y, T_LEN)
print("# y shape: " + str(y.shape))


# save Amino Acid sequences of peptides and save a list of domain names for each peptide:
pep_aa, sh2_domain = data_io_func.get_pep_aa_sh2(testfile, sh2_seq, MAX_PEP_SEQ_LEN)


################################################################################
#   PREDICT SINGLE NETWORKS
################################################################################

# variable to store predictions in:
all_pred = np.zeros((len(ensemble), n_pep))

# go through each net and predict:
# this count parameter is different from the one separating different hyper param configurations
count = 0
old_hyper_params = ''

for paramfile in ensemble:

    # LOAD PARAMETERS:----------------------------------------------------------
    # load parameters of best model:
    best_params = np.load(paramfile)['arr_0']
    ARCHITECTURE = np.load(paramfile)['arr_1']
    hyper_params = np.load(paramfile)['arr_2']

    print("# hyper params")
    print(hyper_params)

    print("# Build network")

    # BUILD NETWORK AND COMPILE TRAINING FUNCTION:------------------------------
    if set(hyper_params) != set(old_hyper_params):
        if ARCHITECTURE == "cnn":

            print("# Building new cnn network")

            N_FEATURES = int(hyper_params[0])
            N_FILTERS = int(hyper_params[1])
            FILTER_SIZES = np.array(hyper_params[2].split(), dtype=int)
            ACTIVATION = hyper_params[3]
            DROPOUT_DENSE = float(hyper_params[4])
            DROPOUT_CNN = float(hyper_params[5])
            N_HID = int(hyper_params[6])
            W_INIT = hyper_params[7]
            if SEPARATE:
                assert hyper_params[8] == 'True'
            else:
                assert hyper_params[8] == 'False'
            if MERGE:
                assert hyper_params[9] == 'True'
            else:
                assert hyper_params[9] == 'False'

            network, in_pep, in_sh2, =\
                NN_func.build_cnn(n_features=N_FEATURES, n_filters=N_FILTERS, filter_sizes=FILTER_SIZES, activation=ACTIVATION,
                                  dropout_dense=DROPOUT_DENSE, dropout_cnn=DROPOUT_CNN, n_hid=N_HID, w_init=W_INIT, separate=SEPARATE,
                                  merge=MERGE)
        else:
            sys.stderr.write("Unknown architecture specified (option -architecture)!\n")
            sys.exit(2)

        # COMPILE PREDICTION FUNCTION-----------------------------------------------
        prediction = lasagne.layers.get_output(network, deterministic=True)

        # compile validation function:
        pred_fn = theano.function([in_pep.input_var, in_sh2.input_var], prediction, allow_input_downcast=True)

    print("# Set weights")
    # SET WEIGHTS---------------------------------------------------------------
    # get current parameters:
    params = lasagne.layers.get_all_param_values(network)

    # check if dimensions match:
    assert len(best_params) == len(params)
    for j in range(0, len(best_params)):
        assert best_params[j].shape == params[j].shape
    # set parameters in network:
    lasagne.layers.set_all_param_values(network, best_params)

    print("# Run forward pass")
    # RUN FORWARD PASS----------------------------------------------------------
    # predict validation set:
    for batch in iterate_minibatches(X_pep, X_sh2, BATCH_SIZE):
        x_pep, x_sh2, excerpt = batch
        all_pred[count][excerpt] = pred_fn(x_pep, x_sh2).flatten()

    old_hyper_params = hyper_params
    count += 1


# calculate mean predictions:
pred = np.mean(all_pred, axis=0)

################################################################################
#   PRINT RESULTS TABLE
################################################################################
print("# Printing results...")
assert pred.shape[0] == y.shape[0] == len(pep_aa) == len(sh2_domain)
outfile = open(outfilename, "w")

outfile.write("peptide\tSH2\tprediction\ttarget\n")
y = y.flatten()
for i in range(0, len(pep_aa)):
    outfile.write(pep_aa[i] + "\t" + sh2_domain[i] + "\t" + str(pred[i]) + "\t" + str(y[i]) + "\n")

# calculate PCC:
pcc, pval = pearsonr(pred.flatten(), y.flatten())
# calculate AUC:
y_binary = y >= 0.5
if all(y_binary) or not any(y_binary):
    auc = "nan"
else:
    auc = roc_auc_score(y_binary, pred.flatten())

outfile.write("# PCC: " + str(pcc) + " p-value: " + str(pval) + " AUC: " + str(auc) + "\n")
outfile.close()

print("# Done!")
