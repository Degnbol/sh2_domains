#!/usr/bin/env python

"""
Functions for specifying convolutional neural network architectures.
"""

from __future__ import print_function
import argparse
import sys
import os
import time

import numpy as np
from scipy.io import netcdf
import theano
import theano.tensor as T

import lasagne

theano.config.floatX = 'float32'


def set_activation(activation):
    '''
    Set activation function

    parameters:
        - activation : string (name of activation function)
    returns:
        - lasagne option
    '''
    # set activation function:
    if activation == "sigmoid":
        return lasagne.nonlinearities.sigmoid
    elif activation == "rectify":
        return lasagne.nonlinearities.rectify
    elif activation == "leaky_rectify":
        return lasagne.nonlinearities.leaky_rectify
    elif activation == "very_leaky_rectify":
        return lasagne.nonlinearities.very_leaky_rectify
    elif activation == "tanh":
        return lasagne.nonlinearities.tanh
    else:
        sys.stderr.write("Unknown activation function (option -activation)!\n")
        sys.exit(2)


def set_w_init(w_init):
    '''
    Set weight initialization

    parameters:
        - w_init : string (name of weight initilization)
    returns:
        - lasagne option
    '''
    if w_init == "normal":
        return lasagne.init.Normal(std=0.05)
    elif w_init == "uniform":
        return lasagne.init.Uniform(range=0.05)
    elif w_init == "constant":
        return lasagne.init.Constant()
    elif w_init == "glorot_normal":
        return lasagne.init.GlorotNormal()
    elif w_init == "glorot_uniform":
        return lasagne.init.GlorotUniform()
    elif w_init == "choice":
        return lasagne.init.Choice(values=[-0.1, 0.1])
    else:
        sys.stderr.write("Unknown weight initialization (option -w_init)!\n")
        sys.exit(2)


# define feed forward network:--------------------------------------------------------
def build_feed_forward(n_features, seq_len, activation, dropout, n_hid, w_init):

    # set activation function:
    nonlin = set_activation(activation)
    # set weight initialization:
    winit = set_w_init(w_init)
    # input layer:
    l_in= lasagne.layers.InputLayer((None, seq_len, n_features))
    # add a dropout layer:
    l_in_drop = lasagne.layers.DropoutLayer(l_in, p=dropout)

    if n_hid > 0:
        # add hidden layer:
        l_hid = lasagne.layers.DenseLayer(l_in_drop, num_units=n_hid, nonlinearity=nonlin, W=winit, b=winit)
    else:
        # just call the unedited input layer the hidden layer and use it.
        l_hid = l_in_drop

    # add a dropout layer:
    l_hid_drop = lasagne.layers.DropoutLayer(l_hid, p=dropout)
    # output layer:
    l_out = lasagne.layers.DenseLayer(l_hid_drop, num_units=1, nonlinearity=lasagne.nonlinearities.sigmoid, W=winit,
                                      b=winit)

    return l_out, l_in


# define LSTM network:-----------------------------------------------------
def build_lstm(n_pep_features, n_sh2_features, n_lstm, activation, dropout, n_hid, w_init, n_pep_len, separate):

    # set activation function:
    nonlin = set_activation(activation)
    # set weight initialization:
    winit = set_w_init(w_init)
    # input layer:
    if separate:
        l_in_pep = lasagne.layers.InputLayer((None, n_pep_len, n_pep_features))
        l_in = lasagne.layers.InputLayer((None, None, n_sh2_features))
    else:
        l_in = lasagne.layers.InputLayer((None, n_pep_len, n_sh2_features))
    # mask layer:
    l_in_mask = lasagne.layers.InputLayer((None, None))
    # LSTM layer:
    l_lstm = lasagne.layers.LSTMLayer(l_in, num_units=n_lstm, peepholes=False, learn_init=True,
                                      nonlinearity=lasagne.nonlinearities.tanh, mask_input=l_in_mask,
                                      only_return_final=True)

    print("# l_lstm dimensions: " + str(l_lstm.output_shape))

    if separate:
        # the layer is reshaped to avoid flattening. It is required that n_lstm is divisible by 21
        l_lstm_reshape = lasagne.layers.ReshapeLayer(l_lstm, ([0], -1, n_pep_features))
        # combine peptide layer with sh2 layer
        l_lstm = lasagne.layers.concat([l_lstm_reshape, l_in_pep], axis=1)
        print("# concatenated dimensions: " + str(l_lstm.output_shape))

    # batch normalization:
    l_lstm_bn = lasagne.layers.batch_norm(l_lstm)
    # hidden layer:
    l_dense = lasagne.layers.DenseLayer(l_lstm_bn, num_units=n_hid, nonlinearity=nonlin, W=winit)
    # batch normalization:
    l_dense_bn = lasagne.layers.batch_norm(l_dense)
    # add a dropout layer:
    l_dense_drop = lasagne.layers.DropoutLayer(l_dense_bn, p=dropout)
    # output layer:
    l_out = lasagne.layers.DenseLayer(l_dense_drop, num_units=1, nonlinearity=lasagne.nonlinearities.sigmoid, W=winit)

    if separate:
        return l_out, l_in_pep, l_in, l_in_mask
    else:
        return l_out, l_in, l_in_mask


# define CNN network:-----------------------------------------------------
# separate is a parameter to indicate if SH2 and peptide are both put through CNN layers. If separate is true, the
# the peptide will not go through the CNN layer and simply be concatenated with SH2 after SH2 goes through CNN.
# merge is a parameter to indicate if SH2 and peptide should be merged from the start, concatenating them and treating
# them as one big encoded group. This will mean that they will share the CNN layer and there will be no concatenation
# between CNN and hidden layers.
def build_cnn(n_features, n_filters, filter_sizes, activation, dropout_dense, dropout_cnn, n_hid, w_init, separate, merge):

    # set activation function:
    nonlin = set_activation(activation)
    # set weight initialization:
    winit = set_w_init(w_init)

    # input layer:
    l_in_pep = lasagne.layers.InputLayer((None, n_features, 13))
    l_in_sh2 = lasagne.layers.InputLayer((None, n_features, None))

    # merge inputs. we need to call this by a new layer name so the input layers remain input type and not concat type
    if merge:
        l_in = lasagne.layers.concat([l_in_pep, l_in_sh2], axis=2)
    else:
        # if we don't merge, the input for the cnn layers will simply be the input layer with SH2
        l_in = l_in_sh2

        if not separate:
            # create lists of cnn/dropout/max pool layers for peptide run through on its own
            l_cnn_pep = []
            l_drop_pep = []
            l_pool_pep = []
    # create lists of cnn/dropout/max pool layers for SH2 or in case of merge, this will be peptide and SH2 combined
    l_cnn = []
    l_drop = []
    l_pool = []

    # fill out the empty lists with layers
    for filter_size in filter_sizes:
        if not merge and not separate:
            # add a CNN layer, a dropout layer and a max pool layer to peptide:
            l_cnn_pep.append(lasagne.layers.Conv1DLayer(l_in_pep, num_filters=n_filters, filter_size=filter_size,
                                                        nonlinearity=lasagne.nonlinearities.tanh))
            l_drop_pep.append(lasagne.layers.DropoutLayer(l_cnn_pep[len(l_cnn_pep)-1], p=dropout_cnn))
            l_pool_pep.append(lasagne.layers.GlobalPoolLayer(l_drop_pep[len(l_drop_pep)-1], pool_function=T.max))

        # add a CNN layer, a dropout layer and a max pool layer to SH2 or in case of merge for SH2+peptide
        l_cnn.append(lasagne.layers.Conv1DLayer(l_in, num_filters=n_filters, filter_size=filter_size,
                                                nonlinearity=lasagne.nonlinearities.tanh))
        l_drop.append(lasagne.layers.DropoutLayer(l_cnn[len(l_cnn)-1], p=dropout_cnn))
        l_pool.append(lasagne.layers.GlobalPoolLayer(l_drop[len(l_drop)-1], pool_function=T.max))

    # print sizes
    if not merge and not separate:
        print("# l_cnn_pep[0] dimensions: " + str(l_cnn_pep[0].output_shape))
        print("# l_drop_pep[0] dimensions: " + str(l_drop_pep[0].output_shape))
        print("# l_pool_pep[0] dimensions: " + str(l_pool_pep[0].output_shape))
    print("# l_cnn[0] dimensions: " + str(l_cnn[0].output_shape))
    print("# l_drop[0] dimensions: " + str(l_drop[0].output_shape))
    print("# l_pool[0] dimensions: " + str(l_pool[0].output_shape))

    # concat pool layers
    if merge:
        l_concat = lasagne.layers.concat(l_pool)
    else:
        if separate:
            # flatten peptide layer to match shape
            l_flat_pep = lasagne.layers.FlattenLayer(l_in_pep, outdim=2)
            # append peptide layer to the SH2 max pool layers
            l_pool.append(l_flat_pep)
            l_concat = lasagne.layers.concat(l_pool)
        else:
            l_concat = lasagne.layers.concat(l_pool_pep + l_pool)
    print("# concatenated dimensions: " + str(l_concat.output_shape))

    # batch normalization:
    l_concat_bn = lasagne.layers.batch_norm(l_concat)
    # hidden layer:
    l_dense = lasagne.layers.DenseLayer(l_concat_bn, num_units=n_hid, nonlinearity=nonlin, W=winit)
    # batch normalization:
    l_dense_bn = lasagne.layers.batch_norm(l_dense)
    # add a dropout layer:
    l_dense_drop = lasagne.layers.DropoutLayer(l_dense_bn, p=dropout_dense)
    # output layer:
    l_out = lasagne.layers.DenseLayer(l_dense_drop, num_units=1, nonlinearity=lasagne.nonlinearities.sigmoid, W=winit)

    return l_out, l_in_pep, l_in_sh2




