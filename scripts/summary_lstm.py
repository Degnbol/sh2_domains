#!/usr/bin/env python

"""
Calculates the Pearson Correlation Coefficient for each combination of hyper parameters to find to
find the best combination. They are collected in a single file that can afterwards be imported into excel to see any
patterns.
"""

import argparse
import sys
import os
import numpy as np
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from sklearn.metrics import roc_auc_score

################################################################################
#  PARSE COMMANDLINE OPTIONS
################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-prediction_file', '--prediction_file',
                    help="File containing all peptides, their prediction and target value")
parser.add_argument('-out', '--outfile',  help="File to store output table")
parser.add_argument('-update', '--update', help="Weight update function used")
parser.add_argument('-activation', '--activation', help="Activation function used in each node")
parser.add_argument('-data_enc', '--data_encoding', help="Encoding of peptides with either sparse or BLOSUM")
parser.add_argument('-lr', '--learning_rate', help="Learning rate used for the models")
parser.add_argument('-n_hid', '--hidden_nodes', help="Number of hidden nodes used")
parser.add_argument('-n_lstm', '--lstm_nodes', help="Number of LSTM nodes used")
parser.add_argument('-n_aa', '--n_aa', help="Number of amino acids presented per time step")
parser.add_argument('-separate', '--separate', help="Whether the input is separated into peptide and SH2 domain")
parser.add_argument('-transform', '--transformation',
                    help="If the transformation used is specified, the values are transformed back to find correlations")
parser.add_argument('-b', '--beta', help="For a sigmoid transformation a beta value can be specified, default=1",
                    default=1)
args = parser.parse_args()

if args.prediction_file is not None:
    prediction_file = args.prediction_file
else:
    sys.stderr.write("Please specify prediction file!\n")
    sys.exit(2)

# get outfile:
if args.outfile is not None:
    outfile = args.outfile
else:
    sys.stderr.write("Please specify output file!\n")
    sys.exit(2)

if args.update is not None:
    update = args.update
else:
    sys.stderr.write("Please specify update function!\n")
    sys.exit(2)

if args.activation is not None:
    activation = args.activation
else:
    sys.stderr.write("Please specify activation function!\n")
    sys.exit(2)

if args.data_encoding is not None:
    data_encoding = args.data_encoding
else:
    sys.stderr.write("Please specify data encoding!\n")
    sys.exit(2)

if args.learning_rate is not None:
    learning_rate = args.learning_rate
else:
    sys.stderr.write("Please specify learning rate!\n")
    sys.exit(2)

if args.hidden_nodes is not None:
    n_hid = args.hidden_nodes
else:
    sys.stderr.write("Please specify number of hidden nodes!\n")
    sys.exit(2)

if args.lstm_nodes is not None:
    n_lstm = args.lstm_nodes
else:
    sys.stderr.write("Please specify number of LSTM nodes!\n")
    sys.exit(2)

if args.n_aa is not None:
    n_aa = args.n_aa
else:
    sys.stderr.write("Please specify n_aa (number of AA per time step)!\n")
    sys.exit(2)

if args.separate is not None:
    separate = args.separate
else:
    sys.stderr.write("Please specify separate!\n")
    sys.exit(2)

# get transformation and possibly beta
if args.transformation is not None:
    if args.transformation == 'sigmoid':
        transform = args.transformation
        if args.beta is not None:
            try:
                beta = float(args.beta)
            except argparse.ArgumentError:
                sys.stderr.write("Problem with beta value!\n")
                sys.exit(2)
        else:
            # this code should never be reachable since there is specified a default value in argparse
            beta = 1
    else:
        sys.stderr.write("Unknown transformation specified!\n")
        sys.exit(2)
else:
    transform = None


# define inverse functions
def inverse_sigmoid(target, beta):

    zscore = target.copy()
    # since it is a piecewise function
    zscore[target <= 0] = 0
    zscore[(target > 0) & (target < 1)] = -np.log(1 / zscore[(target > 0) & (target < 1)] - 1) * beta + 2
    zscore[target >= 1] = max(zscore[(target > 0) & (target < 1)])

    return zscore


################################################################################
#  MAIN
################################################################################

# open file
with open(outfile, 'w') as out:
    # write header
    out.write("SH2\tPCC\tspearman\tAUC\tupdate\tactivation\tdata_encoding\tlearning_rate\thidden_nodes\tlstm_nodes\tn_aa\tseparate\n")

    # collect data from file
    domains = []
    pred = []
    y = []
    with open(prediction_file, 'r') as predfile:
        # skip header
        next(predfile)

        for line in predfile:
            l = line.strip().split()
            domains.append(l[1])
            pred.append(float(l[2]))
            y.append(float(l[3]))

    domains = np.array(domains)
    pred = np.array(pred)
    y = np.array(y)
    assert len(domains) == pred.shape[0] == y.shape[0]

    if transform is not None:
        print("# Transforming prediction and target values back to z-scores.")

        if transform == 'sigmoid':
            print("# using the inverse of sigmoid transformation with beta=" + str(beta) + ".")
            pred = inverse_sigmoid(pred, beta)
            y = inverse_sigmoid(y, beta)

    # unique domains
    uniques = list(set(domains))
    print("# Number of unique domains: " + str(len(uniques)))
    for unique in uniques:

        index = unique == domains
        pred_sub = pred[index]
        y_sub = y[index]

        print("# Calculate PCC, spearman and AUC.")
        # PCC
        pcc, pval = pearsonr(pred_sub.flatten(), y_sub.flatten())
        # spearman
        spear, pval2 = spearmanr(pred_sub.flatten(), y_sub.flatten())
        # AUC
        y_binary = np.where(y_sub >= 0.5, 1, 0)
        auc = roc_auc_score(y_binary.flatten(), pred_sub.flatten())

        # save to file
        out.write(unique + '\t' + str(pcc) + '\t' + str(spear) + '\t' + str(auc) + '\t' + update + '\t' +
                  activation + '\t' + data_encoding + '\t' + learning_rate + '\t' + n_hid + '\t' + n_lstm + '\t' +
                  n_aa + '\t' + separate + '\n')

print("# Done!")





