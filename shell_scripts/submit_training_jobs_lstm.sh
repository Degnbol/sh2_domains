#!/bin/bash

###################################################################################
# GET ARGUMENTS
###################################################################################

echo "Args: " $1
if [ -z "$1" ]; then
   echo "Path to working directory missing!"
   exit
else
    DIR=$1
fi

echo "Args: " $2
if [ -z "$2" ]; then
   echo "Path to data directory missing!"
   exit
else
    DATA=$2
fi

echo "Args: " $3
if [ -z "$3" ]; then
   echo "Path to file with domain names missing!"
   exit
else
    domain_file=$3
fi

echo "Args: " $4
if [ -z "$4" ]; then
   echo "Path to python script missing!"
   exit
else
    script=$4
fi

if [ ! -f $DIR/hyper_params.txt ]; then
    echo "hyper_params.txt not found in working directory!"
    exit
fi

################################################################################
#   MAKE FOLDERS
################################################################################

if [ ! -d $DIR/logs ]; then
  mkdir $DIR/logs
fi
if [ ! -d $DIR/logs/cmds ]; then
  mkdir $DIR/logs/cmds
fi
if [ ! -d $DIR/logs/error ]; then
  mkdir $DIR/logs/error
fi
if [ ! -d $DIR/logs/out ]; then
  mkdir $DIR/logs/out
fi
if [ ! -d $DIR/training ]; then
    mkdir $DIR/training
fi
if [ ! -d $DIR/training/params ]; then
    mkdir $DIR/training/params
fi
if [ ! -d $DIR/training/pred ]; then
    mkdir $DIR/training/pred
fi

################################################################################
#   TRAIN CNNs
################################################################################

seed=0
CVfolds="0 1 2 3 4"

# count is changing to allow for multiple setting in the same hyper param file on separate lines
count=0

while read l; do
    echo "$l"
    # get parameter values
    line=(${l//$'\t'/:})
    IFS=':' read -a h_params <<< "$line"

    nested="${h_params[0]}"
    arch="${h_params[1]}"
    update="${h_params[2]}"
    activation="${h_params[3]}"
    winit="${h_params[4]}"
    data_enc="${h_params[5]}"
    dropout="${h_params[6]}"
    learning_rate="${h_params[7]}"
    n_hid="${h_params[8]}"
    epochs="${h_params[9]}"
    batch_size="${h_params[10]}"
    n_lstm="${h_params[11]}"
    n_aa="${h_params[12]}"
    separate="${h_params[13]}"
    loo="${h_params[14]}"

    if [ "$arch" != "architecture" ];then
        let count=count+1

        if [ "$nested" == "no" ]; then

            # t is iterating over all five testing/evaluation partitions.

            for t in $CVfolds
            do
                echo "t.$t"

                # set seed:
                let seed=seed+1

                # check if training was already finished:
                if [ -s $DIR/training/pred/pred.t.$t.$arch.$count.txt ]; then
                    check=`tail -1 $DIR/training/pred/pred.t.$t.$arch.$count.txt`
                else
                    check="0"
                fi

                if [ "$check" != "# Done!" ]; then

                    rm -f $DIR/training/pred/pred.t.$t.$arch.$count.txt
                    rm -f $DIR/training/params/params.t.$t.$arch.$count.npz

                    # get data:
                    # a little confusing, tdata is training data so that t does not stand for testing.
                    tdata="$DATA/t.$t"
                    vdata="$DATA/c00$t"

                    # ,config.cxx=/usr/bin/g++
                    cmd="OMP_NUM_THREADS=1 \
                            THEANO_FLAGS=blas.ldflags=\"-L/home/projects/vaccine/people/vaju/thinnode_openBLAS/lib/ -lopenblas\" \
                            /home/projects/vaccine/people/vaju/anaconda/bin/python \
                            $script \
                            -training_data $tdata \
                            -validation_data $vdata \
                            -domain_file $domain_file \
                            -encoding $data_enc \
                            -blosum /home/projects/vaccine/people/vaju/DATA/MHCI/BLOSUM50 \
                            -max_pep_seq_length -1 \
                            -epochs $epochs \
                            -model_out $DIR/training/params/params.t.$t.$arch.$count.npz \
                            -print_out $DIR/training/pred/pred.t.$t.$arch.$count.txt \
                            -architecture $arch \
                            -update $update \
                            -activation $activation \
                            -w_init $winit \
                            -dropout $dropout \
                            -learning_rate $learning_rate \
                            -n_hid $n_hid \
                            -n_lstm $n_lstm \
                            -batch_size $batch_size \
                            -seed $seed \
                            -n_aa $n_aa \
                            -loo $loo"

                    if [ "$separate" == "yes" ]; then
                        cmd="$cmd -separate"
                    fi

                    cmd="$cmd >> $DIR/training/pred/pred.t.$t.$arch.$count.txt"

                    rm -f $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh
                    echo "#!/bin/bash" >> $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh
                    echo $cmd >> $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh
                    echo "sleep 60" >> $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh
                    echo "exit 0" >> $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh

                    rm -f $DIR/logs/error/err.t.$t.$arch.$count.txt
                    rm -f $DIR/logs/out/out.t.$t.$arch.$count.txt
                    qsub -W group_list=vaccine -A vaccine -N t.$t.$arch.$count.log -e $DIR/logs/error/err.t.$t.$arch.$count.txt -o $DIR/logs/out/out.t.$t.$arch.$count.txt -l nodes=1:ppn=1:thinnode,mem=10gb,walltime=1:00:00 $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh

                    # wait before submitting next job to give it time to compile:
                    sleep 3

                fi

            done

        elif [ "$nested" == "yes" ]; then

            # t is iterating over all five testing/evaluation partitions. v iterates over validation partitions.
            # they don't iterate over the same number. This means that 20 models are made in total.

            for t in $CVfolds
            do
                for v in $CVfolds
                do
                    if [ "$t" != "$v" ]; then

                        echo "t.$t.v.$v"

                        # set seed:
                        let seed=seed+1

                        # check if training was already finished:
                        if [ -s $DIR/training/pred/pred.t.$t.v.$v.$arch.$count.txt ]; then
                            check=`tail -1 $DIR/training/pred/pred.t.$t.v.$v.$arch.$count.txt`
                        else
                            check="0"
                        fi

                        if [ "$check" != "# Done!" ];then

                            rm -f $DIR/training/pred/pred.t.$t.v.$v.$arch.$count.txt
                            rm -f $DIR/training/params/params.t.$t.v.$v.$arch.$count.npz

                            # get data:
                            # a little confusing, tdata is training data so that t does not stand for testing.
                            tdata="$DATA/t.$t.v.$v"
                            vdata="$DATA/c00$v"


                            # ,config.cxx=/usr/bin/g++
                            cmd="OMP_NUM_THREADS=1 \
                                    THEANO_FLAGS=blas.ldflags=\"-L/home/projects/vaccine/people/vaju/thinnode_openBLAS/lib/ -lopenblas\" \
                                    /home/projects/vaccine/people/vaju/anaconda/bin/python \
                                    $script \
                                    -training_data $tdata \
                                    -validation_data $vdata \
                                    -domain_file $domain_file \
                                    -encoding $data_enc \
                                    -blosum /home/projects/vaccine/people/vaju/DATA/MHCI/BLOSUM50 \
                                    -max_pep_seq_length -1 \
                                    -epochs $epochs \
                                    -model_out $DIR/training/params/params.t.$t.v.$v.$arch.$count.npz \
                                    -architecture $arch \
                                    -update $update \
                                    -activation $activation \
                                    -w_init $winit \
                                    -dropout $dropout \
                                    -learning_rate $learning_rate \
                                    -n_hid $n_hid \
                                    -n_lstm $n_lstm \
                                    -batch_size $batch_size \
                                    -seed $seed \
                                    -n_aa $n_aa"

                            if [ "$separate" == "yes" ]; then
                                cmd="$cmd -separate"
                            fi

                            cmd="$cmd >> $DIR/training/pred/pred.t.$t.v.$v.$arch.$count.txt"

                            rm -f $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh
                            echo "#!/bin/bash" >> $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh
                            echo $cmd >> $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh
                            echo "sleep 60" >> $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh
                            echo "exit 0" >> $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh

                            rm -f $DIR/logs/error/err.t.$t.v.$v.$arch.$count.txt
                            rm -f $DIR/logs/out/out.t.$t.v.$v.$arch.$count.txt
                            qsub -W group_list=vaccine -A vaccine -N t.$t.v.$v.$arch.$count.log -e $DIR/logs/error/err.t.$t.v.$v.$arch.$count.txt -o $DIR/logs/out/out.t.$t.v.$v.$arch.$count.txt -l nodes=1:ppn=1:thinnode,mem=100gb,walltime=12:00:00:00 $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh

                            # wait before submitting next job to give it time to compile:
                            sleep 3
                        fi
                    fi
                done
            done

        fi
    fi
done < $DIR/hyper_params.txt
