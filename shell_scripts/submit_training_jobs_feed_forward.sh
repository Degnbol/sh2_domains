#!/bin/bash

###################################################################################
# GET ARGUMENTS
###################################################################################

echo "Args: " $1
if [ -z "$1" ]; then
   echo "Path to working directory missing!"
   exit
else
    DIR=$1
fi

echo "Args: " $2
if [ -z "$2" ]; then
   echo "Path to data directory missing!"
   exit
else
    DATA=$2
fi

# it is assumed that this file has all domains.
# it is assumed that this file is sh2_domain then a space or tab character for instance then an AA sequence
echo "Args: " $3
if [ -z "$3" ]; then
   echo "Path to domain file or name of domain missing!"
   exit
else
    domain_file=$3
fi

echo "Args: " $4
if [ -z "$4" ]; then
   echo "Path to python script missing!"
   exit
else
    script=$4
fi

if [ ! -f $DIR/hyper_params.txt ]; then
    echo "hyper_params.txt not found in working directory!"
    exit
fi

################################################################################
#   MAKE FOLDERS
################################################################################

if [ ! -d $DIR/logs ]; then
  mkdir $DIR/logs
fi
if [ ! -d $DIR/logs/cmds ]; then
  mkdir $DIR/logs/cmds
fi
if [ ! -d $DIR/logs/error ]; then
  mkdir $DIR/logs/error
fi
if [ ! -d $DIR/logs/out ]; then
  mkdir $DIR/logs/out
fi
if [ ! -d $DIR/domains ]; then
  mkdir $DIR/domains
fi

if [ -f "$domain_file" ]; then
    # run through file and make folders for each domain
    cat "$domain_file" | while read domain sequence; do
        if [ ! -d $DIR/domains/$domain ]; then
            mkdir $DIR/domains/$domain
        fi
        if [ ! -d $DIR/domains/$domain/training ]; then
            mkdir $DIR/domains/$domain/training
        fi
        if [ ! -d $DIR/domains/$domain/training/params ]; then
            mkdir $DIR/domains/$domain/training/params
        fi
        if [ ! -d $DIR/domains/$domain/training/pred ]; then
            mkdir $DIR/domains/$domain/training/pred
        fi
    done
else
    # find a single domain from the input and make a folder for it
    domain="$domain_file"
    if [ ! -d $DIR/domains/$domain ]; then
            mkdir $DIR/domains/$domain
    fi
    if [ ! -d $DIR/domains/$domain/training ]; then
        mkdir $DIR/domains/$domain/training
    fi
    if [ ! -d $DIR/domains/$domain/training/params ]; then
        mkdir $DIR/domains/$domain/training/params
    fi
    if [ ! -d $DIR/domains/$domain/training/pred ]; then
        mkdir $DIR/domains/$domain/training/pred
    fi
fi

################################################################################
#   TRAIN CNNs
################################################################################

seed=0
CVfolds="0 1 2 3 4"

# count is changing to allow for multiple setting in the same hyper param file on separate lines
count=0

while read l; do
    echo "$l"
    # get parameter values
    line=(${l//$'\t'/:})
    IFS=':' read -a h_params <<< "$line"

    nested="${h_params[0]}"
    arch="${h_params[1]}"
    update="${h_params[2]}"
    activation="${h_params[3]}"
    winit="${h_params[4]}"
    data_enc="${h_params[5]}"
    dropout="${h_params[6]}"
    learning_rate="${h_params[7]}"
    n_hid="${h_params[8]}"
    epochs="${h_params[9]}"
    batch_size="${h_params[10]}"

    if [ "$arch" != "architecture" ];then
        let count=count+1

        if [ "$nested" == "no" ]; then

            # t is iterating over all five testing/evaluation partitions.

            for t in $CVfolds
            do
                echo "t.$t"

                # set seed:
                let seed=seed+1

                # get data:
                # a little confusing, tdata is training data so that t does not stand for testing.
                tdata="$DATA/t.$t"
                vdata="$DATA/c00$t"

                # ,config.cxx=/usr/bin/g++
                cmd="OMP_NUM_THREADS=1 \
                        THEANO_FLAGS=blas.ldflags=\"-L/home/projects/vaccine/people/vaju/thinnode_openBLAS/lib/ -lopenblas\" \
                        /home/projects/vaccine/people/vaju/anaconda/bin/python \
                        $script \
                        -training_data $tdata \
                        -validation_data $vdata \
                        -encoding $data_enc \
                        -blosum /home/projects/vaccine/people/vaju/DATA/MHCI/BLOSUM50 \
                        -max_pep_seq_length -1 \
                        -epochs $epochs \
                        -work_dir $DIR/domains \
                        -model_out training/params/params.t.$t.$arch.$count.npz \
                        -print_out training/pred/pred.t.$t.$arch.$count.txt \
                        -architecture $arch \
                        -update $update \
                        -activation $activation \
                        -w_init $winit \
                        -dropout $dropout \
                        -learning_rate $learning_rate \
                        -n_hid $n_hid \
                        -batch_size $batch_size \
                        -seed $seed"

                # see if we are only using a single domain
                if [ -n "$domain" ]; then
                    cmd="$cmd -domain $domain"
                fi

                rm -f $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh
                echo "#!/bin/bash" >> $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh
                echo $cmd >> $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh
                echo "sleep 60" >> $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh
                echo "exit 0" >> $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh

                rm -f $DIR/logs/error/err.t.$t.$arch.$count.txt
                rm -f $DIR/logs/out/out.t.$t.$arch.$count.txt
                qsub -W group_list=vaccine -A vaccine -N t.$t.$arch.$count.log -e $DIR/logs/error/err.t.$t.$arch.$count.txt -o $DIR/logs/out/out.t.$t.$arch.$count.txt -l nodes=1:ppn=1:thinnode,mem=10gb,walltime=1:00:00 $DIR/logs/cmds/train_cmds.t.$t.$arch.$count.sh

                # wait before submitting next job to give it time to compile:
                sleep 3

            done

        elif [ "$nested" == "yes" ]; then

            # t is iterating over all five testing/evaluation partitions. v iterates over validation partitions.
            # they don't iterate over the same number. This means that 20 models are made in total.

            for t in $CVfolds
            do
                for v in $CVfolds
                do
                    if [ "$t" != "$v" ];then

                        echo "t.$t.v.$v"

                        # set seed:
                        let seed=seed+1

                        # get data:
                        # a little confusing, tdata is training data so that t does not stand for testing.
                        tdata="$DATA/t.$t.v.$v"
                        vdata="$DATA/c00$v"


                        # ,config.cxx=/usr/bin/g++
                        cmd="OMP_NUM_THREADS=1 \
                                THEANO_FLAGS=blas.ldflags=\"-L/home/projects/vaccine/people/vaju/thinnode_openBLAS/lib/ -lopenblas\" \
                                /home/projects/vaccine/people/vaju/anaconda/bin/python \
                                $script \
                                -training_data $tdata \
                                -validation_data $vdata \
                                -encoding $data_enc \
                                -blosum /home/projects/vaccine/people/vaju/DATA/MHCI/BLOSUM50 \
                                -max_pep_seq_length -1 \
                                -epochs $epochs \
                                -work_dir $DIR/domains \
                                -model_out training/params/params.t.$t.v.$v.$arch.$count.npz \
                                -print_out training/pred/pred.t.$t.v.$v.$arch.$count.txt \
                                -architecture $arch \
                                -update $update \
                                -activation $activation \
                                -w_init $winit \
                                -dropout $dropout \
                                -learning_rate $learning_rate \
                                -n_hid $n_hid \
                                -batch_size $batch_size \
                                -seed $seed "

                        # see if we are only using a single domain
                        if [ -n "$domain" ]; then
                            cmd="$cmd -domain $domain"
                        fi

                        rm -f $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh
                        echo "#!/bin/bash" >> $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh
                        echo $cmd >> $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh
                        echo "sleep 60" >> $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh
                        echo "exit 0" >> $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh

                        rm -f $DIR/logs/error/err.t.$t.v.$v.$arch.$count.txt
                        rm -f $DIR/logs/out/out.t.$t.v.$v.$arch.$count.txt
                        qsub -W group_list=vaccine -A vaccine -N t.$t.v.$v.$arch.$count.log -e $DIR/logs/error/err.t.$t.v.$v.$arch.$count.txt -o $DIR/logs/out/out.t.$t.v.$v.$arch.$count.txt -l nodes=1:ppn=1:thinnode,mem=10gb,walltime=1:00:00 $DIR/logs/cmds/train_cmds.t.$t.v.$v.$arch.$count.sh

                        # wait before submitting next job to give it time to compile:
                        sleep 3
                    fi
                done
            done

        fi
    fi
done < $DIR/hyper_params.txt
