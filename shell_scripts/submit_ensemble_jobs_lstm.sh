#!/bin/bash

###################################################################################
# GET ARGUMENTS
###################################################################################

echo "Args: " $1
if [ -z "$1" ]; then
   echo "Path to working directory missing!"
   exit
else
    DIR=$1
fi

echo "Args: " $2
if [ -z "$2" ]; then
   echo "Path to data directory missing!"
   exit
else
    DATA=$2
fi

echo "Args: " $3
if [ -z "$3" ]; then
   echo "Path to file with domain names missing!"
   exit
else
    domain_file=$3
fi

echo "Args: " $4
if [ -z "$4" ]; then
   echo "Path to python script missing!"
   exit
else
    script=$4
fi

if [ ! -f $DIR/hyper_params.txt ]; then
    echo "hyper_params.txt not found in working directory!"
    exit
fi

################################################################################
#   MAKE FOLDERS
################################################################################

if [ ! -d $DIR/logs ]; then
  mkdir $DIR/logs
fi
if [ ! -d $DIR/logs/cmds ]; then
  mkdir $DIR/logs/cmds
fi
if [ ! -d $DIR/logs/error ]; then
  mkdir $DIR/logs/error
fi
if [ ! -d $DIR/logs/out ]; then
  mkdir $DIR/logs/out
fi
if [ ! -d $DIR/test ]; then
    mkdir $DIR/test
fi

################################################################################
#   TRAIN CNNs
################################################################################

CVfolds="0 1 2 3 4"

# count is changing to allow for multiple setting in the same hyper param file on separate lines
count=0

while read l; do
    echo "$l"
    # get parameter values
    line=(${l//$'\t'/:})
    IFS=':' read -a h_params <<< "$line"

    nested="${h_params[0]}"
    arch="${h_params[1]}"
    data_enc="${h_params[5]}"
    n_aa="${h_params[12]}"
    separate="${h_params[13]}"
    loo="${h_params[14]}"

    if [ "$arch" != "architecture" ]; then
        let count=count+1

        # t is iterating over all five testing/evaluation partitions. v iterates over validation partitions.
        # they don't iterate over the same number. This means that 20 models are made in total.

        for t in $CVfolds
        do

            echo "t.$t"

            # get data:
            # here the t stands for test
            tdata="$DATA/c00$t"

            # ,config.cxx=/usr/bin/g++
            cmd="OMP_NUM_THREADS=1 \
                    THEANO_FLAGS=blas.ldflags=\"-L/home/projects/vaccine/people/vaju/thinnode_openBLAS/lib/ -lopenblas\" \
                    /home/projects/vaccine/people/vaju/anaconda/bin/python \
                    $script \
                    -test_data $tdata \
                    -domain_file $domain_file \
                    -encoding $data_enc \
                    -blosum /home/projects/vaccine/people/vaju/DATA/MHCI/BLOSUM50 \
                    -max_pep_seq_length -1 \
                    -n_aa $n_aa \
                    -out $DIR/test/t.$t.$arch.$count.txt \
                    -loo $loo \
                    -ensemblelist"

            # add all models for the ensemble one by one to the nargs argument
            if [ "$nested" == "no" ]; then
                cmd="$cmd $DIR/training/params/params.t.$t.$arch.$count.npz"
            elif [ "$nested" == "yes" ]; then
                for v in $CVfolds
                do
                    if [ "$t" != "$v" ]; then
                        cmd="$cmd $DIR/training/params/params.t.$t.v.$v.$arch.$count.npz"
                    fi
                done
            fi

            if [ "$separate" == "yes" ]; then
                cmd="$cmd -separate"
            fi

            rm -f $DIR/logs/cmds/ens_cmds.t.$t.$arch.$count.sh
            echo "#!/bin/bash" >> $DIR/logs/cmds/ens_cmds.t.$t.$arch.$count.sh
            echo $cmd >> $DIR/logs/cmds/ens_cmds.t.$t.$arch.$count.sh
            echo "sleep 60" >> $DIR/logs/cmds/ens_cmds.t.$t.$arch.$count.sh
            echo "exit 0" >> $DIR/logs/cmds/ens_cmds.t.$t.$arch.$count.sh

            rm -f $DIR/logs/error/ens_err.t.$t.$arch.$count.txt
            rm -f $DIR/logs/out/ens_out.t.$t.$arch.$count.txt
            qsub -W group_list=vaccine -A vaccine -N ensemble.t.$t.$arch.$count.log -e $DIR/logs/error/ens_err.t.$t.$arch.$count.txt -o $DIR/logs/out/ens_out.t.$t.$arch.$count.txt -l nodes=1:ppn=1:thinnode,mem=20gb,walltime=3:00:00 $DIR/logs/cmds/ens_cmds.t.$t.$arch.$count.sh
            # wait before submitting next job to give it time to compile:
            sleep 3
        done
    fi
done < $DIR/hyper_params.txt
