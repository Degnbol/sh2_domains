#!/bin/bash

###################################################################################
#  Get arguments
###################################################################################

echo "Args: " $1
if [ -z "$1" ]; then
    echo "Path to main directory missing!"
    exit
else
    DIR=$1
fi

echo "Args: " $2
if [ -z "$2" ]; then
    echo "Path to script missing!"
    exit
else
    script=$2
fi

echo "Args: " $3
if [ -z "$3" ]; then
    echo "No transformation specified so there will be no inverse transformation performed."
else
    transform=$3

    echo "Args: " $4
    if [ -z "$4" ]; then
        echo "No beta given so the default is used for the inverse transformation."
    else
        beta=$4
    fi
fi

echo "Args: " $5
if [ -z "$5" ]; then
    echo "Starting count set as 1."
    start_count=1
else
    echo "Starting with the training in the file specified as number $5"
    start_count=$5
fi

if [ ! -f $DIR/hyper_params.txt ]; then
    echo "hyper_params.txt not found in working directory!"
    exit
fi


################################################################################
#  Main
################################################################################

# count is changing to allow for multiple setting in the same hyper param file on separate lines
count=0

while IFS=$'\t' read -r -a h_params; do

    nested="${h_params[0]}"
    arch="${h_params[1]}"
    update="${h_params[2]}"
    activation="${h_params[3]}"
    data_enc="${h_params[5]}"
    dropout_dense="${h_params[6]}"
    dropout_cnn="${h_params[7]}"
    learning_rate="${h_params[8]}"
    n_hid="${h_params[9]}"
    n_filters="${h_params[12]}"
    filter_sizes="${h_params[13]}"
    separate="${h_params[14]}"
    loo="${h_params[15]}"
    merge="${h_params[16]}"

    if [ "$arch" != "architecture" ];then

        let count=count+1
        echo "count=$count"

        if [ "$count" -ge "$start_count" ]; then

            # collect all predictions in a single file
            # put header into a new collection file
            head -1 $DIR/test/t.0.$arch.$count.txt > $DIR/test/all_$arch.$count.txt

            # add each test set prediction and target value row to the file
            CVfolds="0 1 2 3 4"
            for t in $CVfolds
            do
                cat $DIR/test/t.$t.$arch.$count.txt | grep -v '#' | grep -v 'peptide' >> $DIR/test/all_$arch.$count.txt
            done

            # create summary file with overview of performance

            cmd="OMP_NUM_THREADS=1 \
                    THEANO_FLAGS=blas.ldflags=\"-L/home/projects/vaccine/people/vaju/thinnode_openBLAS/lib/ -lopenblas\" \
                    /home/projects/vaccine/people/vaju/anaconda/bin/python \
                    $script \
                    -prediction_file $DIR/test/all_$arch.$count.txt \
                    -out $DIR/summary.$count.txt \
                    -nested $nested \
                    -update $update \
                    -activation $activation \
                    -data_enc $data_enc \
                    -dropout_dense $dropout_dense \
                    -dropout_cnn $dropout_cnn \
                    -lr $learning_rate \
                    -n_hid $n_hid \
                    -n_filters $n_filters \
                    -filter_sizes $filter_sizes \
                    -separate $separate \
                    -loo $loo \
                    -merge $merge"

            if [ -n "$transform" ]; then
               cmd="$cmd \
               -transform $transform"
            fi

            if [ -n "$beta" ]; then
               cmd="$cmd \
               -beta $beta"
            fi

            rm -f $DIR/logs/cmds/sum_cmds.$count.sh
            echo "#!/bin/bash" >> $DIR/logs/cmds/sum_cmds.$count.sh
            echo $cmd >> $DIR/logs/cmds/sum_cmds.$count.sh
            echo "sleep 60" >> $DIR/logs/cmds/sum_cmds.$count.sh
            echo "exit 0" >> $DIR/logs/cmds/sum_cmds.$count.sh
            rm -f $DIR/logs/error/sum_err.$count.txt
            rm -f $DIR/logs/out/sum_out.$count.txt

            qsub -W group_list=vaccine -A vaccine -N sum.$arch.$count.log -e $DIR/logs/error/sum_err.$count.txt -o $DIR/logs/out/sum_out.$count.txt -l nodes=1:ppn=1:thinnode,mem=10gb,walltime=1:00:00 $DIR/logs/cmds/sum_cmds.$count.sh

            # wait before submitting next job to give it time to compile
            sleep 3
        fi
    fi
done < $DIR/hyper_params.txt




