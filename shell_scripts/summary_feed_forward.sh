#!/bin/bash

###################################################################################
#  Get arguments
###################################################################################

echo "Args: " $1
if [ -z "$1" ]; then
    echo "Path to main directory missing!"
    exit
else
    DIR=$1
fi

echo "Args: " $2
if [ -z "$2" ]; then
    echo "Path to script missing!"
    exit
else
    script=$2
fi

echo "Args: " $3
if [ -z "$3" ]; then
    echo "No transformation specified so there will be no inverse transformation performed."
else
    transform=$3

    echo "Args: " $4
    if [ -z "$4" ]; then
        echo "No beta given so the default is used for the inverse transformation."
    else
        beta=$4
    fi
fi

if [ ! -f $DIR/hyper_params.txt ]; then
    echo "hyper_params.txt not found in working directory!"
    exit
fi


################################################################################
#  Main
################################################################################

# count is changing to allow for multiple setting in the same hyper param file on separate lines
count=0

while read l; do
    # get parameter values
    line=(${l//$'\t'/:})
    IFS=':' read -a h_params <<< "$line"

    nested="${h_params[0]}"
    arch="${h_params[1]}"
    data_enc="${h_params[5]}"
    learning_rate="${h_params[7]}"
    n_hid="${h_params[8]}"

    if [ "$arch" != "architecture" ];then

        let count=count+1


        # collect all predictions in a single file

        for TESTDIR in $DIR/domains/*/test
        do
            # put header into a new collection file
            head -1 $TESTDIR/t.0.$arch.$count.txt > $TESTDIR/all_$arch.$count.txt

            # add each test set prediction and target value row to the file
            CVfolds="0 1 2 3 4"
            for t in $CVfolds
            do
                cat $TESTDIR/t.$t.$arch.$count.txt | grep -v '#' | grep -v 'peptide' >> $TESTDIR/all_$arch.$count.txt
            done
        done

        # create summary file with overview of performance for each SH2 domain

        cmd="OMP_NUM_THREADS=1 \
                THEANO_FLAGS=blas.ldflags=\"-L/home/projects/vaccine/people/vaju/thinnode_openBLAS/lib/ -lopenblas\" \
                /home/projects/vaccine/people/vaju/anaconda/bin/python \
                $script \
                -work_dir $DIR/domains \
                -prediction_file test/all_$arch.$count.txt \
                -out $DIR/summary.$count.txt \
                -nested $nested \
                -data_enc $data_enc \
                -lr $learning_rate \
                -n_hid $n_hid"

        if [ -n "$transform" ]; then
           cmd="$cmd \
           -transform $transform"
        fi

        if [ -n "$beta" ]; then
           cmd="$cmd \
           -beta $beta"
        fi

        rm -f $DIR/logs/cmds/sum_cmds.$count.sh
        echo "#!/bin/bash" >> $DIR/logs/cmds/sum_cmds.$count.sh
        echo $cmd >> $DIR/logs/cmds/sum_cmds.$count.sh
        echo "sleep 60" >> $DIR/logs/cmds/sum_cmds.$count.sh
        echo "exit 0" >> $DIR/logs/cmds/sum_cmds.$count.sh
        rm -f $DIR/logs/error/sum_err.$count.txt
        rm -f $DIR/logs/out/sum_out.$count.txt

        qsub -W group_list=vaccine -A vaccine -N sum.$arch.$count.log -e $DIR/logs/error/sum_err.$count.txt -o $DIR/logs/out/sum_out.$count.txt -l nodes=1:ppn=1:thinnode,mem=10gb,walltime=1:00:00 $DIR/logs/cmds/sum_cmds.$count.sh

        # wait before submitting next job to give it time to compile
        sleep 3
    fi
done < $DIR/hyper_params.txt




