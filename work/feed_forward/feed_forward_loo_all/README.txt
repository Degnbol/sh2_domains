################################################################################
#	EXPERIMENT SETUP
################################################################################

cat /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/nearest_neighbours | while read predict_domain train_domain identity; do

# make folder:

mkdir work/feed_forward/feed_forward_loo_all/$predict_domain

# prepare hyper parameter file:

echo -e "nested\tarchitecture\tupdate\tactivation\tweight_init\tdata_encoding\tdropout\tlearning_rate\tn_hidden\tepochs\tbatch_size" > work/feed_forward/feed_forward_loo_all/$predict_domain/hyper_params.txt
echo -e "no\tfeed_forward\tsgd\tsigmoid\tchoice\tblosum\t0.0\t0.2\t15\t500\t20" >> work/feed_forward/feed_forward_loo_all/$predict_domain/hyper_params.txt

done

################################################################################
#  TRAIN NET
################################################################################

cat /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/nearest_neighbours | while read predict_domain train_domain identity; do

./shell_scripts/submit_training_jobs_feed_forward.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/feed_forward/feed_forward_loo_all/$predict_domain \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  $train_domain \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/train_feed_forward.py

done

################################################################################
#  PREDICT NETS
################################################################################

cat /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/nearest_neighbours | while read predict_domain train_domain identity; do

./shell_scripts/submit_ensemble_jobs_feed_forward.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/feed_forward/feed_forward_loo_all/$predict_domain \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/ensemble_feed_forward.py \
  $predict_domain

done

################################################################################
#  Summary
################################################################################

cat /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/nearest_neighbours | while read predict_domain train_domain identity; do

./shell_scripts/summary_feed_forward.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/feed_forward/feed_forward_loo_all/$predict_domain \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/summary_feed_forward.py

done


# collect all these summaries in the main folder and add only that to the git and use that for R overview work
cd /home/projects/vaccine/people/s134801/sh2_domains/work/feed_forward/feed_forward_loo_all
head -1 ABL1/summary.1.txt > summary.1.txt
for subfolder in */; do tail -1 $subfolder/summary.1.txt >> summary.1.txt; done
cd -



