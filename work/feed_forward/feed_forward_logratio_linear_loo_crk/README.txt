################################################################################
#	EXPERIMENT SETUP
################################################################################

# prepare hyper parameter file:

echo -e "nested\tarchitecture\tupdate\tactivation\tweight_init\tdata_encoding\tdropout\tlearning_rate\tn_hidden\tepochs\tbatch_size" > work/feed_forward/feed_forward_logratio_linear_loo_crk/hyper_params.txt
echo -e "yes\tfeed_forward\tsgd\tsigmoid\tchoice\tblosum\t0.0\t0.2\t15\t500\t20" >> work/feed_forward/feed_forward_logratio_linear_loo_crk/hyper_params.txt

################################################################################
#  TRAIN NET
################################################################################

./shell_scripts/submit_training_jobs_feed_forward.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/feed_forward/feed_forward_logratio_linear_loo_crk \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  CRKL \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/train_feed_forward.py

################################################################################
#  PREDICT NETS
################################################################################

./shell_scripts/submit_ensemble_jobs_feed_forward.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/feed_forward/feed_forward_logratio_linear_loo_crk \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/ensemble_feed_forward.py \
  CRK

################################################################################
#  Summary
################################################################################

./shell_scripts/summary_feed_forward.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/feed_forward/feed_forward_logratio_linear_loo_crk \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/summary_feed_forward.py


