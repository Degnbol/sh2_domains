################################################################################
#	EXPERIMENT SETUP
################################################################################

# prepare hyper parameter file:

echo -e "architecture\tupdate\tactivation\tweight_init\tdata_encoding\tdropout\tlearning_rate\tn_hidden\tepochs\tbatch_size" > work/feed_forward/feed_forward_sigmoid_1/hyper_params.txt
echo -e "feed_forward\tsgd\tsigmoid\tchoice\tblosum\t0.0\t0.05\t20\t500\t20" >> work/feed_forward/feed_forward_sigmoid_1/hyper_params.txt
echo -e "feed_forward\tsgd\tsigmoid\tchoice\tblosum\t0.0\t0.05\t15\t500\t20" >> work/feed_forward/feed_forward_sigmoid_1/hyper_params.txt
echo -e "feed_forward\tsgd\tsigmoid\tchoice\tblosum\t0.0\t0.1\t20\t500\t20" >> work/feed_forward/feed_forward_sigmoid_1/hyper_params.txt
echo -e "feed_forward\tsgd\tsigmoid\tchoice\tblosum\t0.0\t0.1\t15\t500\t20" >> work/feed_forward/feed_forward_sigmoid_1/hyper_params.txt

################################################################################
#  TRAIN NET
################################################################################

./shell_scripts/submit_training_jobs_feed_forward.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/feed_forward/feed_forward_sigmoid_1 \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_DATA3/partitions/sigmoid_1 \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/train_feed_forward.py \
  hyper_params.txt

################################################################################
#  PREDICT NETS
################################################################################

./shell_scripts/submit_ensemble_jobs_feed_forward.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/feed_forward/feed_forward_sigmoid_1 \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_DATA3/partitions/sigmoid_1 \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/ensemble_feed_forward.py \
  hyper_params.txt

################################################################################
#  Summary
################################################################################

./shell_scripts/summary_feed_forward.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/feed_forward/feed_forward_sigmoid_1 \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/summary_feed_forward.py \
  hyper_params.txt \
  sigmoid
