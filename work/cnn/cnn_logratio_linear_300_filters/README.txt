################################################################################
#	EXPERIMENT SETUP
################################################################################

# prepare hyper parameter file:

echo -e "nested\tarchitecture\tupdate\tactivation\tweight_init\tdata_encoding\tdropout\tlearning_rate\tn_hidden\tepochs\tbatch_size\tn_filters\tfilter_sizes" > work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t15\t50\t20\t200\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t15\t50\t20\t300\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t25\t50\t20\t200\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t25\t50\t20\t300\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t35\t50\t20\t200\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t35\t50\t20\t300\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t60\t50\t20\t200\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t60\t50\t20\t300\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t120\t50\t20\t200\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t120\t50\t20\t300\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t200\t50\t20\t200\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t200\t50\t20\t300\t3 5 7" >> work/cnn/cnn_logratio_linear_300_filters/hyper_params.txt



################################################################################
#  TRAIN NET
################################################################################

./shell_scripts/submit_training_jobs_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_logratio_linear_300_filters \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/train_cnn.py

################################################################################
#  PREDICT NETS
################################################################################

./shell_scripts/submit_ensemble_jobs_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_logratio_linear_300_filters \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/ensemble_cnn.py

################################################################################
#  Summary
################################################################################

./shell_scripts/summary_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_logratio_linear_300_filters \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/summary_cnn.py
