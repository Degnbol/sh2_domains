################################################################################
#	EXPERIMENT SETUP
################################################################################

# prepare hyper parameter file:

echo -e "nested\tarchitecture\tupdate\tactivation\tweight_init\tdata_encoding\tdropout_dense\tdropout_cnn\tlearning_rate\tn_hidden\tepochs\tbatch_size\tn_filters\tfilter_sizes\tseparate\tloo\tmerge" > work/cnn/cnn_alanine_scan_gads/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0\t0.0001\t250\t40\t20\t250\t3 5 7\tno\tNone\tno" >> work/cnn/cnn_alanine_scan_gads/hyper_params.txt

# instead of running training, copy params from the configs to a training/params folder you make. rename them to have the correct $count value

################################################################################
#  PREDICT NETS
################################################################################

./shell_scripts/submit_ensemble_jobs_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_alanine_scan_gads \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/src_alanine_scan \
  "" \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/ensemble_cnn.py


################################################################################
#  SUMMARY
################################################################################


# collect all predictions in a single file
# put header into a new collection file
cd /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_alanine_scan_gads/test
for count in {1..4}; do
head -1 t.0.cnn.$count.txt > all_cnn.$count.txt
# add each test set prediction and target value row to the file
for t in 0 1 2 3 4; do
cat t.$t.cnn.$count.txt | grep -v '#' | grep -v 'peptide' >> all_cnn.$count.txt
done
done
cd -

# then use the R doc
