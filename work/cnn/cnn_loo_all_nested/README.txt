################################################################################
#	EXPERIMENT SETUP
################################################################################

# this is testing two hyper params for all domains except for the ones already tested.
# prepare hyper parameter file:

echo -e "nested\tarchitecture\tupdate\tactivation\tweight_init\tdata_encoding\tdropout_dense\tdropout_cnn\tlearning_rate\tn_hidden\tepochs\tbatch_size\tn_filters\tfilter_sizes\tseparate\tloo" > work/cnn/cnn_loo_all_nested/hyper_params.txt

cat /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains | while read col1 col2; do
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0\t0.0001\t500\t40\t20\t500\t3 5 7\tno\t$col1" >> work/cnn/cnn_loo_all_nested/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0\t0.0001\t250\t40\t20\t250\t3 5 7\tno\t$col1" >> work/cnn/cnn_loo_all_nested/hyper_params.txt
done

################################################################################
#  TRAIN NET
################################################################################

./shell_scripts/submit_training_jobs_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_loo_all_nested \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/train_cnn.py

################################################################################
#  PREDICT NETS
################################################################################

./shell_scripts/submit_ensemble_jobs_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_loo_all_nested \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/ensemble_cnn.py

################################################################################
#  Summary
################################################################################

./shell_scripts/summary_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_loo_all_nested \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/summary_cnn.py
