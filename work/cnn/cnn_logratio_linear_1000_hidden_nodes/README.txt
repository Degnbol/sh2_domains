################################################################################
#	EXPERIMENT SETUP
################################################################################

# prepare hyper parameter file:

echo -e "nested\tarchitecture\tupdate\tactivation\tweight_init\tdata_encoding\tdropout_dense\tdropout_cnn\tlearning_rate\tn_hidden\tepochs\tbatch_size\tn_filters\tfilter_sizes\tseparate" > work/cnn/cnn_logratio_linear_1000_hidden_nodes/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0\t0.0001\t500\t50\t20\t450\t3 5 7\tno" >> work/cnn/cnn_logratio_linear_1000_hidden_nodes/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0\t0.0001\t1000\t50\t20\t450\t3 5 7\tno" >> work/cnn/cnn_logratio_linear_1000_hidden_nodes/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.1\t0.0\t0.0001\t500\t50\t20\t450\t3 5 7\tno" >> work/cnn/cnn_logratio_linear_1000_hidden_nodes/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.1\t0.0\t0.0001\t1000\t50\t20\t450\t3 5 7\tno" >> work/cnn/cnn_logratio_linear_1000_hidden_nodes/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.2\t0.0\t0.0001\t500\t50\t20\t450\t3 5 7\tno" >> work/cnn/cnn_logratio_linear_1000_hidden_nodes/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.2\t0.0\t0.0001\t1000\t50\t20\t450\t3 5 7\tno" >> work/cnn/cnn_logratio_linear_1000_hidden_nodes/hyper_params.txt



################################################################################
#  TRAIN NET
################################################################################

./shell_scripts/submit_training_jobs_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_logratio_linear_1000_hidden_nodes \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/train_cnn.py

################################################################################
#  PREDICT NETS
################################################################################

./shell_scripts/submit_ensemble_jobs_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_logratio_linear_1000_hidden_nodes \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/ensemble_cnn.py

################################################################################
#  Summary
################################################################################

./shell_scripts/summary_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_logratio_linear_1000_hidden_nodes \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/summary_cnn.py
