################################################################################
#	EXPERIMENT SETUP
################################################################################

# prepare hyper parameter file:

echo -e "nested\tarchitecture\tupdate\tactivation\tweight_init\tdata_encoding\tdropout\tlearning_rate\tn_hidden\tepochs\tbatch_size\tn_filters\tfilter_sizes" > work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tsgd\tsigmoid\tchoice\tsparse\t0.0\t0.05\t20\t50\t20\t20\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tsgd\tsigmoid\tchoice\tsparse\t0.0\t0.05\t20\t50\t20\t40\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tsgd\tsigmoid\tchoice\tsparse\t0.0\t0.05\t20\t50\t20\t100\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tsgd\tsigmoid\tchoice\tsparse\t0.0\t0.05\t30\t50\t20\t20\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tsgd\tsigmoid\tchoice\tsparse\t0.0\t0.05\t30\t50\t20\t40\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tsgd\tsigmoid\tchoice\tsparse\t0.0\t0.05\t30\t50\t20\t100\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t10\t50\t20\t20\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t10\t50\t20\t40\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t10\t50\t20\t100\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t15\t50\t20\t20\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t15\t50\t20\t40\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t15\t50\t20\t100\t3 5 7" >> work/cnn/cnn_logratio_linear_100_filters/hyper_params.txt



################################################################################
#  TRAIN NET
################################################################################

./shell_scripts/submit_training_jobs_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_logratio_linear_100_filters \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/train_cnn.py

################################################################################
#  PREDICT NETS
################################################################################

./shell_scripts/submit_ensemble_jobs_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_logratio_linear_100_filters \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/ensemble_cnn.py

################################################################################
#  Summary
################################################################################

./shell_scripts/summary_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_logratio_linear_100_filters \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/summary_cnn.py
