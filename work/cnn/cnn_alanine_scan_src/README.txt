################################################################################
#	EXPERIMENT SETUP
################################################################################

# prepare hyper parameter file:

echo -e "nested\tarchitecture\tupdate\tactivation\tweight_init\tdata_encoding\tdropout_dense\tdropout_cnn\tlearning_rate\tn_hidden\tepochs\tbatch_size\tn_filters\tfilter_sizes\tseparate\tloo" > work/cnn/cnn_alanine_scan_src/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0\t0.0001\t500\t40\t20\t450\t3 5 7\tno\tNone" >> work/cnn/cnn_alanine_scan_src/hyper_params.txt
echo -e "yes\tcnn\tadam\trectify\tchoice\tblosum\t0.0\t0.0\t0.0001\t300\t40\t20\t250\t3 5 7\tno\tNone" >> work/cnn/cnn_alanine_scan_src/hyper_params.txt


################################################################################
#  PREDICT NETS
################################################################################

./shell_scripts/submit_ensemble_jobs_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_alanine_scan_src \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/src_alanine_scan \
  "" \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/ensemble_cnn.py

################################################################################
#  Summary
################################################################################

./shell_scripts/summary_cnn.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/cnn/cnn_alanine_scan_src \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/summary_cnn.py
