################################################################################
#	EXPERIMENT SETUP
################################################################################

# prepare hyper parameter file:

echo -e "nested\tarchitecture\tupdate\tactivation\tweight_init\tdata_encoding\tdropout\tlearning_rate\tn_hidden\tepochs\tbatch_size\tn_lstm\tn_aa\tseparate" > work/lstm/lstm_logratio_linear/hyper_params.txt
echo -e "yes\tlstm\tsgd\tsigmoid\tchoice\tblosum\t0.0\t0.05\t20\t50\t20\t20\t1\tno" >> work/lstm/lstm_logratio_linear/hyper_params.txt
echo -e "yes\tlstm\tsgd\tsigmoid\tchoice\tblosum\t0.0\t0.05\t20\t50\t20\t100\t1\tno" >> work/lstm/lstm_logratio_linear/hyper_params.txt
echo -e "yes\tlstm\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t20\t50\t20\t20\t1\tno" >> work/lstm/lstm_logratio_linear/hyper_params.txt
echo -e "yes\tlstm\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t20\t50\t20\t20\t3\tno" >> work/lstm/lstm_logratio_linear/hyper_params.txt
echo -e "yes\tlstm\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t20\t50\t20\t50\t1\tno" >> work/lstm/lstm_logratio_linear/hyper_params.txt
echo -e "yes\tlstm\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t20\t50\t20\t50\t3\tno" >> work/lstm/lstm_logratio_linear/hyper_params.txt
echo -e "yes\tlstm\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t20\t50\t20\t100\t1\tno" >> work/lstm/lstm_logratio_linear/hyper_params.txt
echo -e "yes\tlstm\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t20\t50\t20\t100\t3\tno" >> work/lstm/lstm_logratio_linear/hyper_params.txt
echo -e "yes\tlstm\tadam\trectify\tchoice\tblosum\t0.0\t0.0001\t30\t50\t20\t100\t1\tno" >> work/lstm/lstm_logratio_linear/hyper_params.txt
echo -e "yes\tlstm\tadam\trectify\tchoice\tsparse\t0.0\t0.0001\t20\t50\t20\t100\t1\tno" >> work/lstm/lstm_logratio_linear/hyper_params.txt


################################################################################
#  TRAIN NET
################################################################################

./shell_scripts/submit_training_jobs_lstm.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/lstm/lstm_logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/train_lstm.py

################################################################################
#  PREDICT NETS
################################################################################

./shell_scripts/submit_ensemble_jobs_lstm.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/lstm/lstm_logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/partitions/logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/data/SH2_martin/domains/domains \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/ensemble_lstm.py

################################################################################
#  Summary
################################################################################

./shell_scripts/summary_lstm.sh \
  /home/projects/vaccine/people/s134801/sh2_domains/work/lstm/lstm_logratio_linear \
  /home/projects/vaccine/people/s134801/sh2_domains/scripts/summary_lstm.py
